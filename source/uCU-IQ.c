/*****************************************************
This program was produced by the
CodeWizardAVR V2.05.4 Standard
Automatic Program Generator
� Copyright 1998-2011 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project : uCU-IQ
Version : V38.015 
Date    : 25.09.2019
Author  : Jiri Votocek
Company : ICAS CS s.r.o.
Comments: 

     v38.001: 28/02/2017  0-Batch First release
     v38.002: 28/08/2017  EEPROM delete fix, BACKBONE communication test fix   
     v38.003: 14/11/2017  Delay function added after request from Norway 
     v38.004: 16/11/2017  Hush stops delay transmition - removes F from buffer
     v38.005: 14/12/2017  RFevents save fix
     v38.006: 01/02/2018  Filter for communication fault events
     v38.007: 09/02/2018  Fix of the switching off the Delay retransmiting, fix of the message parameters for quit message.
     v38.008: 28/03/2018  Fix of EN54-25 - Indications, Level access, Time of Radio module waking up.
     v38.009: 19/04/2018  Fix of communication fault - all faults are displayed after testing period - need of 31.00I in IQRF
     v38.010: 14/05/2018  Fix of Alarm Counter save - In case the alarm from the node is already received, the message F is not incremented until reset or stop is done.    
     v38.011: 13/06/2018  Fix of the ALARM relay switching.
     v38.012: 25/09/2018  RESET function transfered into level II - push RST / disable function - hold RST for 2 second.  
     v38.013: 12/10/2018  Fix of the Q message silencing through the system, fix of the BackboneEventRAM index setting, during Alarm ANY COMM FAULT is not indicated.
     v38.013+: 03/12/2018  EVPU requirements - check of memory integrity and WDT testing / Field test comments - Relay faults 
     v38.014: 22/03/2019  Tronds field tests - Power Line testing low voltage fixed, System Comm check fix, WDT reset from timer 2 fixed  
                          6s delay before RST+TEST action acknowledgement, Test button in unconfigured control unit does not cause WDR.
     v38.015: 25/09/2019  Tronds requirement - Comm Loss Switching off. Comm Loss detection changed - each node has its own filter timer.   
     v38.015: 25/09/2019  Tronds requirement - Comm Loss Switching off. Comm Loss detection changed - each node has its own filter timer.
     v38.016:             Tronds requirement - Level 3 enter changed - 5s wait when TEST + SIREN pressed at the same time.
     v38.016:             Fix of WDT reset event EEPROM write - event '1' in EEPROM
     v38.017:             With I log message is also sent the setting of uCUIQ(Fire delay, comm loss filter, EN5425 setting)
          
Chip type               : ATmega16A
Program type            : Application
AVR Core Clock frequency: 4,000000 MHz
Memory model            : Small
External RAM size       : 0
Data Stack size         : 256
*****************************************************/

#include <mega16a.h>
#include <delay.h>



#define RF_BUTTON       PORTA.1 //
#define RF_BUTTON_IN    PINA.1 //
#define SS_LED          PORTA.2 //SS enable for SPI bus to the LED driver - EN = Rising Edge   
#define SS_RF           PORTA.3 //SS enable for SPI bus to the RF module - EN = Low level / DISABLE = High level   
#define RST_LED         PORTB.1 //RST for the LED driver - RST = High / EN = Low
#define POWER_MAINS     PINC.4 //Indicator if mains is conected - High = No mains / Low = Mains connected
#define POWER_FAULT     PINC.3 //Indicator if fault appears - High = Fault / Low = No Fault
#define RELE_ALARM      PORTC.5 //Alarm RElAY enable - High = ON / Low = OFF
#define RELE_FAULT      PORTD.7 //Fault RElAY enable - High = ON / Low = OFF    
#define TL3             PIND.4 //PANIC BUTTON
#define TL2             PIND.5 //RST_OFF BUTTON
#define TL1             PIND.6 //SIREN BUTTON
#define RS485_RX        PIND.0 //RS485 receiver - Low = Receiving message / High = No message
#define RS485_TXEN      PORTD.2 //RS485 transmiter enable - Low = Receiver is enabled / High = Transmiter is enabled
//#define TL              ((TL1<<2)|(TL2<<1)|(TL3<<0))
#define TL              ((TL1<<2)|(TL2<<1)|(TL3))
//#define TL.1            TL2
//#define TL.0            TL3
#define MOSI            PORTB.5

#define SET_INT_SIREN_SW    PORTB.4 = 1                      /////////*********************////////
#define CLR_INT_SIREN_SW    PORTB.4 = 0                      /////////*********************////////
#define NEG_INT_SIREN_SW    PORTB.4 ^= 1                     /////////*********************////////
//
#define POWER_LED           7//6
#define L_ALARM_LED         6//5
#define R_ALARM_LED         5//4
#define BATTERY_LED         4//3
#define L_BATTERY_LED       3//2
#define LEVEL2_LED          2//1
#define RF_L_FAULT_LED      1//0
#define RF_B_FAULT_LED      0//7
#define RS485_FAULT_LED     14
#define HUSH_LED            13
#define TEST_LED            12 
#define DISABLE_LED         11
#define SYS_FAULT_LED       10
#define P_FAULT_LED         9
#define FAULT_LED           8
////
//#define POWER_LED           6//0
//#define L_ALARM_LED         5//1
//#define R_ALARM_LED         4//2
//#define BATTERY_LED         3//3
//#define L_BATTERY_LED       2//4
//#define LEVEL2_LED          1//5
//#define RF_L_FAULT_LED      0//6
//#define RF_B_FAULT_LED      7//7
//#define RS485_FAULT_LED     14//8
//#define HUSH_LED            13//9
//#define TEST_LED            12//10 
//#define DISABLE_LED         11//11
//#define SYS_FAULT_LED       10//12
//#define P_FAULT_LED         9//13
//#define FAULT_LED           8//14

/*#define POWER_LED           0
#define L_ALARM_LED         1
#define R_ALARM_LED         2
#define BATTERY_LED         3
#define L_BATTERY_LED       4
#define LEVEL2_LED          5
#define RF_L_FAULT_LED      6
#define RF_B_FAULT_LED      7
#define RS485_FAULT_LED     8
#define HUSH_LED            9
#define TEST_LED            10 
#define DISABLE_LED         11
#define SYS_FAULT_LED       12
#define P_FAULT_LED         13
#define FAULT_LED           14
*/
#define POWER_bit           0
#define L_ALARM_bit         1
#define R_ALARM_bit         2
#define TESTMODE_bit        3
#define L_BATTERY_bit       4
#define LEVEL2_bit          5
#define RF_L_FAULT_bit      6
#define RF_B_FAULT_bit      7
#define RS485_FAULT_bit     8
#define HUSH_bit            9
#define TEST_bit            10 
#define DISABLE_bit         11
#define SYS_FAULT_bit       12
#define P_FAULT_bit         13
#define FAULT_bit           14

#define Fire_Alarm_bit      0
#define Fault_Alarm_bit     1
#define Battery_Fault_bit   2
#define Hush_bit            3
#define Test_bit            4
#define Disable_bit         5
//#define Comm_Fault_bit      6
#define Power_Fault_bit     7

#define RS485_Fire_Alarm_bit      Fire_Alarm_bit
#define RS485_comm_det_bit        1
#define RS485_Fault_Alarm_bit     2
#define RS485_Bat_Fault_bit       3
        
#define BitTest(a,b)            ((a>>b)&(0x0001))
#define BitSet(a,b)             a = (a|(1<<b))
#define BitRST(a,b)             a = (a&(0xFFFF^(1<<b)))
#define BitNeg(a,b)             a = (a^(0x0000|(1<<b)))

#define PFilter1_timer_END      2
#define PFilter2_timer_END      2

#define SPI_CHECK           0x00 
#define SPI_CMD             0xF0
#define PTYPE               0x80

#define SPI_ID              0
#define SPI_SUBLOOP         1
#define SPI_ZONE            2
#define SPI_COMMAND         3

#define RS485_timer_MAX     3            // 300/16
#define RS485_time2ANSWER   1
#define DFAULT_ModulationKonst      4    // 1s
#define TEST_ModulationKonst        8    // 2s

// (Jp) constant of delay for entry into Level3
#define L3_delay                    10   // 10 cycles
                                      
#define BFAULT_ModulationKonst      32   // 8s
#define SFAULT_ModulationKonst      32   // 8s
#define PFAULT_ModulationKonst      64   // 15s
#define BACKFAULT_ModulationKonst   128  // 30s 
#define RS485FAULT_ModulationKonst  128  // 30s
#define COMFAULT_ModulationKonst    128  // 30s
#define HUSH_ModulationKonst        128  // 30s 
#define Max_Delay_RFtransmition     240  // 4min //Calculation for EEPROM ((Max_Delay_RFtransmition/Delay_RFtransmition)-1)
#define Delay_RFtransmition         60   // Delay_RFtransmition*250ms = 15s
#define Delay_9min_RFtransmition    9    // 9min
#define MemoryIntegrityTestTimeout  20   // 20min
#define MAXcommFaultFilter          10   // 10cycles - one cycle 6 min 
#define TESTtimeout                 2    // 1 minute 
#define HUSHtimeout                 12   //
#define InstallationTestTimeout     5    // 3 minute
#define SystemCommTestTimeout       6    // 6 min
#define RS485CommTestTimeout        6    // 6 min    
#define ModulationLED               5
#define Clock2Division              8
#define LearnModeTimerMAX   100
#define Reset_CounterMAX    100
#define RFtimeoutKonst      12           //aprox. 1 minute - 55.78s  -> 16s + 6.63s*6
#define RFresetKonst        2           //32s
//#define RFmoduleBusyKonst   1           //16s
#define EEPROM_store        0x10
#define EEStartsNMB         0
#define EEnmbRS485          1
#define EEadrRS485          2
#define EEnmbRFEvent        3
#define EEcarryRFEvent      4
#define EEadrHRFEvent       5
#define EEadrLRFEvent       6
#define EEdelay2TX	        7
#define EEcommFaultFilter   8
#define EEsettingEN54_25    9
#define EEoriginalCRC       10

#define EEPROMRFEventDATA   0x20
#define EEmaxRFEvent        400
#define LearnMode           0
#define BackboneLearnMode   1
#define RemoteLearnMode     2
#define LearnModeExit       3
#define Back2Level1Const    2
#define settingEN54_25Konst 1
#define RS485_TimingMin     2
#define TestMode            0
#define RFattenuation       1
#define TestInstallation    2

#define PowerReset          0
#define BrownReset          1
#define WDTReset            2
#define UnwantedRestart     3
#define RestartWriteEnable  4

void SPI_Transfer2LED(unsigned int data);
unsigned int CheckEventStatus(unsigned int data);
unsigned int Indication(unsigned int input);
unsigned int Indication2(unsigned int input);
void button_pressed(unsigned char *level,unsigned char *mode);
unsigned char button_function_level1(unsigned char butt,unsigned char level);
unsigned char button_function_level2(unsigned char butt,unsigned char level);
unsigned char button_function_level3(unsigned char butt,unsigned char level);
unsigned char button_function_level4(unsigned char butt,unsigned char level);
void Power_indication_filter();
unsigned char SPI_Transfer2RF(unsigned char SPIDLEN);
void SPI_message2CPU_Decoding(void);
void ResetCPU(void);
//void ResetAlarmFault(void);
unsigned char USART_Receive(void);
void RS485Message_Receive(void);
void RS485Message_Decode(void);
void RS485Message_DecodeIQ(void);
unsigned char RS485Message_Construct(unsigned char T_ID,unsigned char T_SUB,unsigned char T_ZONE,unsigned char dataComm);
//void USART_Transmit(unsigned char data);
void RS485Message_Transmit(void);
unsigned char RS485Comm_Test(unsigned char adr2Test);
void RFmoduleSwitchingON(void);
void CPUstartDelay(void);
void EEPROM_write(unsigned int addr, unsigned char data);
unsigned char EEPROM_read(unsigned int addr);
void delay_msWDR(unsigned char MS);
void RFEventSave(unsigned char E_ID,unsigned char E_SUBLOOP,unsigned char E_ZONE, unsigned char E_COMMAND);
void RFEventHistoryOUT(void);
void RFEventsDelete();
void Check_Memory_Integrity(void);
void ResetPanelTest(void);
unsigned char TestEvent2Fix(unsigned char T_ID,unsigned char T_SUB,unsigned char T_ZONE,unsigned char T_COMMAND);
unsigned char TestEvent1Fix(unsigned char T_ID);
// Declare your global variables here
bit blink_LED,silence,RFprogramng,Loop_Comm_Fault,Backbone_Comm_Fault,RS485_Comm_Fault;
bit gotRS485_message,RS485TestEN,RS485Send,DoLearnReset,RFunprogramed,RFmoduleComunication;
bit RFmoduleRST;
//bit RFmoduleBusy;
bit RS485_IQnet;
bit ProductionMode;

#asm(".dseg")  
#asm(".org 0x0100")
volatile unsigned int LED_Register          @0x0100;
volatile unsigned int LED_Register = 0x0000;
volatile unsigned int Panel_Status          @0x0102;
volatile unsigned int RS485_timer           @0x0104;
volatile unsigned int Reset_Counter         @0x0106;
volatile unsigned int BackBoneTest          @0x0108;
volatile unsigned char LearnStatus          @0x010A;
volatile unsigned int Clock                 @0x010B;
volatile unsigned char Clock2               @0x010D;
volatile unsigned char Clock3               @0x010E; 
volatile unsigned char BackBoneNMB          @0x010F;
volatile unsigned char DetNodeNMB           @0x0110;
volatile unsigned char NodeID_origin        @0x0111;
volatile unsigned char SystemID_origin      @0x0112;
volatile unsigned char ZoneID_origin        @0x0113;
volatile unsigned char LoopEventRAM[32]     @0x0114;//0x134
volatile unsigned char BackboneEventRAM[32] @0x0135;//0x155
volatile unsigned char RS485EventRAM[15]    @0x0156;//0x165
volatile unsigned char messageRS485[32]     @0x0166;//0x186
volatile unsigned char button               @0x0187;
volatile unsigned char pushbutton_pressed   @0x0188;
volatile unsigned char button_old           @0x0189;
volatile unsigned char Event                @0x018A;
volatile unsigned char PM_Filter1_timer     @0x018B;
volatile unsigned char PF_Filter2_timer     @0x018C;
volatile unsigned char PM_Filter1           @0x018D;
volatile unsigned char PF_Filter2           @0x018E;
volatile unsigned char timeout              @0x018F;
volatile unsigned char LearnModeTimer       @0x0190;
volatile unsigned char RFtimeout            @0x0191;
volatile unsigned char modulatorSIREN       @0x0192;
volatile unsigned char RS485_maxADR         @0x0193;
volatile unsigned char RS485_IQnetADR       @0x0194;
volatile unsigned char RS485_MessageIndex   @0x0195;
volatile unsigned char RS485_MessageRepeat  @0x0196;
volatile unsigned char messageLength        @0x0197;
volatile unsigned char SPI2RFok             @0x0198;
volatile unsigned char SPIRXbufferPointer   @0x0199;
volatile unsigned char SPI_message2RF[32]   @0x019A;//0x1BA
volatile unsigned char SPI_RXbuffer[32]     @0x01BB;//0x1DB
volatile unsigned char SPI_message2CPU[32]  @0x01DC;//0x1FC
volatile unsigned char RFmoduleStarting     @0x01FD;
volatile unsigned char Level3               @0x01FE;
volatile unsigned char CommandExpiration    @0x01FF;
volatile unsigned char Programing_Mode      @0x0200;
volatile unsigned char Line1                @0x0201;
volatile unsigned char Line2                @0x0202;
volatile unsigned char ToLevel1Timeout      @0x0203;
volatile unsigned char DelayedTransmitionTimeout @0x0204;
volatile unsigned char DelayedTransmitionEN @0x0205;
volatile unsigned char DelayTXminKonst      @0x0206;
volatile unsigned char DelayTXmin           @0x0207;
volatile unsigned char DelayTXminKonstQ     @0x0208;
volatile unsigned char commFaultStored1[4]   @0x0209;//0x20D
volatile unsigned char commFaultStored2[4] @0x020E;//0x212
volatile unsigned char commFaultFilter1     @0x0213;
volatile unsigned char RecCommBuffer        @0x0214;
volatile unsigned char commFaultRegister[4] @0x0215;//0x219
volatile unsigned char settingEN54_25       @0x0220;
volatile unsigned char commTest             @0x0221;
volatile unsigned char RS485NewMessage2Send @0x0222; 
volatile unsigned char RS485_Timing         @0x0223;
//volatile unsigned char testMode;
//volatile unsigned char testRFattenuation;
volatile unsigned char testRegister         @0x0224;
volatile unsigned char maxBackBoneIDdoNotWork @0x0225;
volatile unsigned char MemTestEN            @0x0226;
volatile unsigned char Actual_memory_CRC    @0x0227;
volatile unsigned char Original_memory_CRC  @0x0228;
volatile unsigned char ZalohaZ[4]           @0x0229;//0x022D
volatile unsigned char RestartCauses        @0x022E;
volatile unsigned char MemoryTestTimer      @0x022F;
volatile unsigned char commFaultFilter2     @0x0230;
volatile unsigned char SystemTestTimer      @0x0231;
volatile unsigned char messageRS485TX[32]   @0x0232;//0x252
volatile unsigned char TestIND  @0x0252;
volatile unsigned char commFaultNodeMem[4]  @0x0253;
volatile unsigned char commFaultSysMem      @0x0257;
volatile unsigned char commFaultTimer1[32]  @0x0258;//0x0258 - 0x0278
volatile unsigned char commFaultTimer2[8]   @0x0279;//0x0279 - 0x0281

//(Jp) counter of delay for entry into Level3;
volatile unsigned char L3Cnt             @0x0282; //0x282 -0x283

//volatile unsigned char Panel_LoopID;
//volatile unsigned char Panel_SubloopID;
//volatile unsigned char Panel_ZoneID;
//volatile unsigned char Command2RF;
// External Interrupt 2 service routine

// Timer 0 overflow interrupt service routine
interrupt [TIM0_OVF] void timer0_ovf_isr(void)
{
// Place your code here
    #asm("cli")
    TIFR=0x00;
    TCCR0=0x00; 
    TCNT0=0x00;
    timeout = 1;
    #asm("sei")
}

// Timer1 overflow interrupt service routine
interrupt [TIM1_OVF] void timer1_ovf_isr(void)
{
    // Reinitialize Timer1 value
    #asm("cli") 
    TIFR=0x00;
    TCNT1H=0x85;
    TCNT1L=0xF0;
    TCCR1B=0x00;
    // Place your code here
    timeout = 1;  
    #asm("sei") 
}

interrupt [EXT_INT2] void ext_int2_isr(void)
{
    #asm("cli")
    GICR=0x00;
    GIFR=0x00;
    if (TL<0x07){
        pushbutton_pressed = 1;
    }
    if (RS485_RX == 0){
        gotRS485_message = 1;   
        pushbutton_pressed = 0;
    }
    #asm("sei")
}

// RTC overflow interrupt service routine
interrupt [TIM2_OVF] void timer2_ovf_isr(void)
{
    unsigned char Konst4Test;
    unsigned char i,y,a;
    #asm("cli")     
    TIMSK&=0x3F;
    #asm("nop")
    #asm("nop")
    #asm("nop")
    //ASSR=0x00; 
    TCNT2=0x00;
    TCCR2=0x00;
    while((ASSR&0x07)>0);
    TIFR=0x00;
    TIMSK&=0x3F;
    if (Clock<0x1FE){                      //Clock reset 510 * 31,25ms = 15937ms => 16s
        ++Clock;
    }           
    else{
        Clock = 0;        
    }
    /*if (Clock<0xFF){                      //Clock reset 255 * 62,5ms = 15937ms => 16s
        ++Clock;
    }           
    else{
        Clock = 0;        
    } */ 
    if (Clock%Clock2Division==0){        //62.5ms*4 => 250ms
        if (Clock2<0xFF){                //Clock2 reset 250ms * 255 = 63750ms => 64s = 1min
            ++Clock2;      
        }           
        else{
            Clock2 = 0;                 
        }       
    }
    if ((Clock2==0)&&(Clock == 0)){      //1 min  
        if (Clock3<0xF0){                //Clock3 reset 255 * 1min = 240min => 4h
            ++Clock3;
        }           
        else{
            Clock3 = 0;
        }
    }  
            
    if (LearnModeTimer < LearnModeTimerMAX){  
        ++LearnModeTimer;                       
    } 
    if (LearnModeTimer == (LearnModeTimerMAX-1)){
        SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
        if (BitTest(LearnStatus,BackboneLearnMode) == 1){
            SPI_message2RF[SPI_SUBLOOP] = 0;
        } 
        BitSet(LearnStatus,LearnModeExit);
        SPI_message2RF[SPI_ID] = NodeID_origin;
        SPI_message2RF[SPI_ZONE] = ZoneID_origin; 
        SPI_message2RF[SPI_COMMAND] = 'L';
    }
    if (SPIRXbufferPointer>0){   
        if (Clock%Clock2Division==0){        //62.5ms*4 => 250ms       
            if (DelayedTransmitionTimeout<Delay_RFtransmition){                //Clock2 reset 250ms * 60 => 15s
                ++DelayedTransmitionTimeout;
            }           
            else{
                DelayedTransmitionTimeout = 0;
                ++DelayTXmin;
                if(DelayTXminKonstQ > 0){
                    Konst4Test = DelayTXminKonstQ;
                }
                else{
                    Konst4Test = DelayTXminKonst;
                }
                if (DelayTXmin >= Konst4Test){ 
                    DelayedTransmitionEN = 1;
                    DelayTXmin = 0;  
                    DelayTXminKonstQ = 0;        
                }
            }
        }                             
    }  
    else{
        DelayedTransmitionTimeout=0;
    }
     
    /*if ((Clock == 0)&&(RFtimeout<0xFF)){
    	++RFtimeout;  
    }*/
    if ((Clock%0x7F)<0x0F){
        blink_LED = 1;
    }
    else{
        blink_LED = 0;
    } 
    /*
    if ((blink_LED == 0)&&(Clock%0x7F==0)){
        blink_LED = 1;
    }
    else{
        blink_LED = 0;
    } 
    */  
    if (BitTest(testRegister,TestInstallation) == 1){
        if (Clock3 == InstallationTestTimeout){    
            SPI_message2RF[SPI_ID] = NodeID_origin;
            SPI_message2RF[SPI_ZONE] = ZoneID_origin;
            SPI_message2RF[SPI_SUBLOOP] = SystemID_origin; 
            SPI_message2RF[SPI_COMMAND] = 'T';
            BitRST(testRegister,TestInstallation);
            BitSet(LoopEventRAM[SPI_message2RF[SPI_ID]],Test_bit);
            silence=0;
        }
    }
    
    if (RFmoduleComunication == 1){
        if ((Clock == 0)&&(RFtimeout<0xFF)){
            ++RFtimeout;  
        }      
    }
    else if (RFmoduleRST == 1){
        if ((Clock == 0)&&(RFtimeout<0xFF)){
            ++RFtimeout;  
        }      
    }
    else{
        RFtimeout = 0;
    }
    
    if (MemoryTestTimer == MemoryIntegrityTestTimeout){
        MemoryTestTimer = 0;                           
        MemTestEN = 1;
    }
    else if ((MemoryTestTimer < MemoryIntegrityTestTimeout)&&((Clock2 == 0)&&(Clock == 0))){
        ++MemoryTestTimer;
    }                     
    
    if ((RS485_timer < RS485_timer_MAX)&&(Clock==0)){ //increment is done once per 16s
        ++RS485_timer;                     
    }
    
    if (PF_Filter2_timer<(PFilter2_timer_END+1)){
        PF_Filter2_timer++;
    }                    
    else{
        PF_Filter2_timer = 0;
    }
    
    if (PM_Filter1_timer<(PFilter1_timer_END+1)){
        PM_Filter1_timer++;
    }                    
    else{
        PM_Filter1_timer = 0;
    } 
    
    if ((Level3 != 0)||(BitTest(Panel_Status,LEVEL2_bit)!=0)){
        if (Clock3==Back2Level1Const){
            //++ToLevel1Timeout;
            //if (ToLevel1Timeout == Back2Level1Const){
            //    ToLevel1Timeout = 0;
            BitRST(Panel_Status,LEVEL2_bit);
            Level3 = 0;    
            //}    
        }                                            
    }
    /*else{
        ToLevel1Timeout = 0;
    } */
    //SPI_Transfer2LED(test);   
    //delay_ms(20);
    #asm("sei")
}
void main(void)
{
// Declare your local variables here
unsigned char level, mode,i; 
unsigned char RS485_dev2TEST;

// Init value of the counter - (The Delay for entry Level3 is after entry Level2 only)
L3Cnt = 0;

// Input/Output Ports initialization
// Port A initialization
// Func7=In Func6=In Func5=In Func4=In Func3=Out Func2=Out Func1=Out Func0=In 
// State7=T State6=T State5=T State4=T State3=0 State2=0 State1=0 State0=T 
PORTA=0x00;
DDRA=0x0E;

//// Port B initialization
//// Func7=Out Func6=In Func5=Out Func4=Out Func3=In Func2=In Func1=Out Func0=In 
//// State7=0 State6=P State5=0 State4=0 State3=T State2=T State1=0 State0=T 
//PORTB=0x40;
//DDRB=0xB2;

// Port B initialization
// Func7=Out Func6=In Func5=Out Func4=Out Func3=In Func2=In Func1=Out Func0=In 
// State7=0 State6=T State5=0 State4=0 State3=T State2=T State1=0 State0=T 
PORTB=0x00;
DDRB=0xB2;

// Port C initialization
// Func7=In Func6=In Func5=Out Func4=In Func3=In Func2=In Func1=In Func0=In 
// State7=T State6=T State5=0 State4=T State3=T State2=T State1=T State0=T 
PORTC=0x00;
DDRC=0x20;

// Port D initialization
// Func7=Out Func6=In Func5=In Func4=In Func3=In Func2=Out Func1=Out Func0=In 
// State7=0 State6=P State5=P State4=P State3=T State2=0 State1=0 State0=T 
PORTD=0x70;
DDRD=0x86;

// Timer/Counter 0 initialization
// Clock source: System Clock
// Clock value: Timer 0 Stopped
// Mode: Normal top=0xFF
// OC0 output: Disconnected
TCCR0=0x00;
TCNT0=0x00;
OCR0=0x00;

// Timer/Counter 1 initialization
// Clock source: System Clock
// Clock value: Timer1 Stopped
// Mode: Normal top=0xFFFF
// OC1A output: Discon.
// OC1B output: Discon.
// Noise Canceler: Off
// Input Capture on Falling Edge
// Timer1 Overflow Interrupt: Off
// Input Capture Interrupt: Off
// Compare A Match Interrupt: Off
// Compare B Match Interrupt: Off
TCCR1A=0x00;
TCCR1B=0x00;
TCNT1H=0x00;
TCNT1L=0x00;
ICR1H=0x00;
ICR1L=0x00;
OCR1AH=0x00;
OCR1AL=0x00;
OCR1BH=0x00;
OCR1BL=0x00;

// Timer/Counter 2 initialization
// Clock source: System Clock
// Clock value: Timer2 Stopped
// Mode: Normal top=0xFF
// OC2 output: Disconnected
ASSR=0x00;
TCCR2=0x00;
TCNT2=0x00;
OCR2=0x00;

// External Interrupt(s) initialization
// INT0: Off
// INT1: Off
// INT2: Off
GICR=0x00;
MCUCR=0x00;
//MCUCSR=0x00;
GIFR=0x00;

// Timer(s)/Counter(s) Interrupt(s) initialization
TIMSK=0x00;

//// USART initialization
//// Communication Parameters: 8 Data, 2 Stop, No Parity
//// USART Receiver: On
//// USART Transmitter: On
//// USART Mode: Asynchronous
//// USART Baud Rate: 4800
//UCSRA=0x00;
//UCSRB=0x18;
//UCSRC=0x8E;
//UBRRH=0x00;
//UBRRL=0x33;

// USART initialization
// Communication Parameters: 8 Data, 2 Stop, No Parity
// USART Receiver: On
// USART Transmitter: On
// USART Mode: Asynchronous
// USART Baud Rate: 4800
UCSRA=0x00;
UCSRB=0x58;
UCSRC=0x8E;
UBRRH=0x00;
UBRRL=0x31;   //zm�na z 0x33 4600 Bd na 0x31 - komunikace s uCU-M.485

// Analog Comparator initialization
// Analog Comparator: Off
// Analog Comparator Input Capture by Timer/Counter 1: Off
ACSR=0x80;
SFIOR=0x00;

// ADC initialization
// ADC disabled
ADCSRA=0x00;

// SPI initialization
// SPI disabled
SPCR=0x00;

// TWI initialization
// TWI disabled
TWCR=0x00;
//MemTestEN = 1;
//Check_Memory_Integrity();
ResetCPU();
ResetPanelTest();
RS485_dev2TEST = 0;
RS485_MessageIndex=0; 
CLR_INT_SIREN_SW;
TestIND = 0;
if (TL2 == 0){
    while(TL2 == 0){
        SPI_Transfer2LED(0x0001<<POWER_LED);
        delay_msWDR(50);
        SPI_Transfer2LED(0x0001<<L_ALARM_LED);
        delay_msWDR(50);
        SPI_Transfer2LED(0x0001<<R_ALARM_LED);
        delay_msWDR(50);
        SPI_Transfer2LED(0x01<<BATTERY_LED);
        delay_msWDR(50);
        SPI_Transfer2LED(0x01<<L_BATTERY_LED);
        delay_msWDR(50);
        SPI_Transfer2LED(0x01<<LEVEL2_LED);
        delay_msWDR(50);
        SPI_Transfer2LED(0x01<<RF_L_FAULT_LED);
        delay_msWDR(50);
        SPI_Transfer2LED(0x01<<RF_B_FAULT_LED);
        delay_msWDR(50);
        SPI_Transfer2LED(0x01<<RS485_FAULT_LED);
        delay_msWDR(50);
        SPI_Transfer2LED(0x01<<HUSH_LED);
        delay_msWDR(50);
        SPI_Transfer2LED(0x01<<TEST_LED);
        delay_msWDR(50);
        SPI_Transfer2LED(0x01<<DISABLE_LED);
        delay_msWDR(50);
        SPI_Transfer2LED(0x01<<SYS_FAULT_LED);
        delay_msWDR(50);
        SPI_Transfer2LED(0x01<<P_FAULT_LED); 
        delay_msWDR(50);
        SPI_Transfer2LED(0x01<<FAULT_LED); 
        delay_msWDR(50);
    }
    ProductionMode = 1;                   
}
else{
    ProductionMode = 0;
    Reset_Counter = EEPROM_read(EEPROM_store+EEStartsNMB);
    if (Reset_Counter == 0x00){
        Check_Memory_Integrity();
        Original_memory_CRC = Actual_memory_CRC;
        EEPROM_write(EEPROM_store+EEoriginalCRC,Original_memory_CRC);
    }
    if (Reset_Counter < 0xFF){
        ++Reset_Counter;
        EEPROM_write(EEPROM_store+EEStartsNMB,Reset_Counter);
    }
}
SPI_Transfer2LED(0xFFFF);
delay_ms(100);      
SPI_Transfer2LED(0x0000);
delay_ms(100);
SPI_Transfer2LED(0xFFFF);
delay_ms(100);      
SPI_Transfer2LED(0x0000);
delay_ms(100);
while (!RFprogramng)
    {       
        #asm("wdr");
        WDTCR=(1<<WDTOE) | (1<<WDE) | (1<<WDP2) | (1<<WDP1) | (1<<WDP0);
        WDTCR=(0<<WDTOE) | (1<<WDE) | (1<<WDP2) | (1<<WDP1) | (1<<WDP0);
        #asm("wdr");
        TestIND = 0;
        RF_BUTTON=0; 
        if (RS485_Timing < RS485_TimingMin){
            ++RS485_Timing;   
        }
        RS485Message_Receive();
        TestIND=1;    //1
        Power_indication_filter(); 
        TestIND=2;   //2  
        
        button_pressed(&level,&mode);
         
        TestIND=3;    //3                              
        Check_Memory_Integrity(); 
        TestIND=4;     //4
        if (DelayedTransmitionEN == 1){
            for (i = 0;i<4;++i){
            	SPI_message2RF[i] = SPI_RXbuffer[i];
            }
            SPI_message2RF[SPI_COMMAND] = SPI_message2RF[SPI_COMMAND]-0x20;
            if ((SPI_message2RF[SPI_COMMAND] == 'F')||(SPI_message2RF[SPI_COMMAND] == 'S')){
            	SPI_message2RF[SPI_ZONE]=0;
            } 
        }
        TestIND=5; //5
        //SPI_Transfer2LED(0x04);   
        //delay_ms(50);
        if (BitTest(Panel_Status,DISABLE_bit) == 0){   
            for(i = 0;i<32;++i){
                SPI_message2CPU[i] = 0;
            }
            SPI2RFok = 0;
            //#asm("wdr"); 
            SPI2RFok = SPI_Transfer2RF(4); 
            TestIND=20;//6        
            if (SPI2RFok == 0x3F){
                /*if (SPI_message2RF[SPI_COMMAND] == 'L'){             
                    pom ^= 1;
                } */
                /*if ((SPI_message2RF[SPI_COMMAND] == 'L')&&(BitTest(LearnStatus,LearnMode) == 0)){
                    DoLearnReset = 1;  
                    LearnStatus = 0; 
                    Reset_Counter = 0;    
                } */ 
                RFmoduleComunication = 0; 
                if (DelayedTransmitionEN == 1){
                	DelayedTransmitionEN = 0;
                    i = EEPROM_read(EEPROM_store+EEdelay2TX);
                    if (i > Delay_9min_RFtransmition){  
                        DelayedTransmitionEN = 0xFF;
	                }  
                    for(i = 0;i<SPIRXbufferPointer;++i){
                        if	((i+4)<32){
                            SPI_RXbuffer[i] = SPI_RXbuffer[i+4];
                        }   
                        else{
                            SPI_RXbuffer[i] = 0;
                        }
                    } 
                    for(i = SPIRXbufferPointer-4;i<SPIRXbufferPointer;++i){
            	        SPI_RXbuffer[i] = 0;
                    }
                    SPIRXbufferPointer -= 4;
                } 
                TestIND=21; //7
                SPI_message2CPU_Decoding();
                TestIND=22; //8 
                SPI_message2RF[SPI_ID] = NodeID_origin;
                SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
                SPI_message2RF[SPI_ZONE] = ZoneID_origin;  
                SPI_message2RF[SPI_COMMAND] = 'N'; 
            }
            else if (RFmoduleRST == 0){                      
                RFmoduleComunication = 1;       
            }                        
            else
            {
                RFmoduleComunication = 0;   
            }
        }
        else{
            RFmoduleComunication = 0;  
        }
        //SPI_Transfer2LED(0x08);   
        //delay_ms(30);
        if (Event == 1){  
            TestIND=6;    //9
            Event = 0;    
            LED_Register = 0;
            Panel_Status = CheckEventStatus(Panel_Status);
            TestIND=7;    //10
            if (Level3 == 0){
                LED_Register = Indication(Panel_Status);
                TestIND=8;  //11
            }         
            else if (Level3 == 1){              
                LED_Register = Indication2(RS485_maxADR);
                TestIND=9;  //12
                //BitSet(LED_Register,RS485_maxADR);
            } 
            else{                                       
                LED_Register = Indication2(RS485_IQnetADR); 
                TestIND=10;   //13
                //BitSet(LED_Register,RS485_IQnetADR);
            } 
            //#asm("wdr");
            SPI_Transfer2LED(LED_Register); 
            TestIND=11;      //14
        }
        //SPI_Transfer2LED(0x10);   
        //delay_ms(50);
        RS485_dev2TEST = RS485Comm_Test(RS485_dev2TEST); 
        TestIND=12;//15
        //SPI_Transfer2LED(0x20);   
        //delay_ms(50);
        if (RS485_RX != 0){
            RS485Message_Transmit();
        }
        TestIND=13;//16
        //SPI_Transfer2LED(0x40);   
        //delay_ms(50);  
        RF_BUTTON=1;
        if (RS485Send == 0){  
            if (RFmoduleComunication == 0){
                #asm("cli") 
                TIMSK&=0x3F;
                #asm("nop")
                #asm("nop")
                #asm("nop")  
                /*
                //Sleep for 62,5ms//
                ASSR=0x08; 
                TCNT2=0x00;
                OCR2=0x00;
                TCCR2=0x02;         //Time setting Clock/8 = 32768 / 8 = 4096        
                while((ASSR&0x07)>0);  
                //---------------//  
                */
                /*
                //Sleep for 31,25ms//
                ASSR=0x08; 
                TCNT2=0x80;
                OCR2=0x00;
                TCCR2=0x02;         //Time setting Clock/8 = 32768 / 8 = 4096        
                while((ASSR&0x07)>0);  
                //---------------//
                */                  
                TestIND=14;//18
                //Sleep for 41,25ms//
                //ASSR=0x08; 
                TCNT2=0x57;
                OCR2=0x00;
                TCCR2=0x02;         //Time setting Clock/8 = 32768 / 8 = 4096        
                while((ASSR&0x07)>0);
                //---------------//
                TestIND=15;//19   
                TIFR|=0xC0;
                TIMSK|=0x40;
                GICR|=0x20;
                GIFR=0x20;
                #asm("nop")
                #asm("nop") 
                #asm("nop")
               //SPI_Transfer2LED(2);          
                #asm("sei")   
                MCUCR = 0x30;
                MCUCR |= 0x40; 
                #asm("sleep")       
                #asm("nop")
                #asm("nop")
            } 
            else{
                #asm("cli")
                TIMSK&=0x3F;
                #asm("nop")
                #asm("nop")
                #asm("nop")
                TestIND=16;//20
                //Sleep for 13ms//
                //ASSR=0x08; 
                TCNT2=0xD1;
                OCR2=0x00;
                TCCR2=0x02;         //Time setting Clock/8 = 32768 / 8 = 4096      
                while((ASSR&0x07)>0);
                //---------------//
                TestIND=17;//21
                /*
                //Sleep for 31,25ms//
                ASSR=0x08; 
                TCNT2=0x80;
                OCR2=0x00;
                TCCR2=0x02;         //Time setting Clock/8 = 32768 / 8 = 4096           
                while((ASSR&0x07)>0);
                //---------------//
                */          
                TIFR|=0xC0;
                TIMSK|=0x40;
                GICR|=0x20;
                GIFR=0x20;
                #asm("nop")
                #asm("nop") 
                #asm("nop")
                #asm("sei")
                MCUCR = 0x30;
                MCUCR |= 0x40; 
                #asm("sleep")       //Sleep for 26ms 
                #asm("nop")
                #asm("nop")
            }
        }
        else{         
            #asm("cli")
            TIMSK&=0x3F;
            #asm("nop")
            #asm("nop")
            #asm("nop")
            TestIND = 18;
            //ASSR=0x08; 
            TCNT2=0xFE;
            OCR2=0x00;
            TCCR2=0x02;         //Time setting Clock/8 = 32768 / 8 = 4096           
            while((ASSR&0x07)>0);
            TestIND = 19;           
            TIFR=0x00;
            TIMSK|=0x40;
            GICR|=0x20;
            GIFR=0x20;
            #asm("nop")
            #asm("nop") 
            #asm("nop")
            #asm("sei")
            if (RFmoduleComunication == 0){
                delay_msWDR(31); 
                //delay_msWDR(62); 
            }
            else{ 
                delay_msWDR(13);
                //delay_msWDR(31);
            }
        }       
    }
while(1);      
}

// Voltage Reference: AVCC pin
#define ADC_VREF_TYPE ((0<<REFS1) | (1<<REFS0) | (1<<ADLAR))

void init_adc()
{
    // ADC initialization
    // ADC Clock frequency: 250,000 kHz
    // ADC Voltage Reference: AVCC pin
    // ADC Auto Trigger Source: Free Running
    // Only the 8 most significant bits of
    // the AD conversion result are used
    ADMUX=ADC_VREF_TYPE;
    ADCSRA=(1<<ADEN) | (0<<ADSC) | (1<<ADATE) | (0<<ADIF) | (0<<ADIE) | (1<<ADPS2) | (0<<ADPS1) | (0<<ADPS0);
    SFIOR=(0<<ADTS2) | (0<<ADTS1) | (0<<ADTS0);
}

void off_adc()
{
    ADCSRA=0x00;
}

// Read the 8 most significant bits
// of the AD conversion result
unsigned char read_adc(unsigned char adc_input)
{
    ADMUX=adc_input | ADC_VREF_TYPE;
    // Delay needed for the stabilization of the ADC input voltage
    delay_us(10);
    // Start the AD conversion
    ADCSRA|=(1<<ADSC);
    // Wait for the AD conversion to complete
    while ((ADCSRA & (1<<ADIF))==0);
    ADCSRA|=(1<<ADIF);
    return ADCH;
}

void ResetCPU(void){
unsigned char i,x;
    CLR_INT_SIREN_SW;  
    #asm("cli") 
    TIMSK=0x00;
    #asm("nop")
    #asm("nop")
    #asm("nop")  
    ASSR=0x08; 
    TCNT2=0x00;
    OCR2=0x00;
    TCCR2=0x02;                
    while((ASSR&0x07)>0);
    CPUstartDelay(); 
    //ASSR=0x00;
    TCCR2=0x00;
    TCNT2=0x00;
    OCR2=0x00;
    while((ASSR&0x07)>0);
    TIFR=0x00;
    TIMSK=0x00;
    GICR|=0x20;
    MCUCR=0x00;
    GIFR=0x20; 
    ////Global bits RESET////
    //RFmoduleBusy = 0;
    RFmoduleRST = 0;
    blink_LED = 0;
    silence = 0;
    RFprogramng = 0;
    Loop_Comm_Fault = 0;
    Backbone_Comm_Fault = 0;
    RS485_Comm_Fault = 0;
    gotRS485_message = 0; 
    RS485TestEN = 0;
    RS485Send = 0;  
    DoLearnReset = 0;
    RFunprogramed = 1;
    RFmoduleComunication = 0;
    RS485_IQnet = 0; 
    ProductionMode = 0;
    /////////////////////////
    //////Global Variables RESET//////      
    LED_Register = 0;
    Panel_Status = 0;                      
    RS485_timer = 0;
    Reset_Counter = 0;
    BackBoneTest = 0;
    LearnStatus = 0;
    Clock = 0;
    Clock2 = 0;
    Clock3 = 0; 
    SystemTestTimer = 1;
    BackBoneNMB = 1;    //Minimal count of system
    DetNodeNMB = 0;
    NodeID_origin = 1;
    SystemID_origin = 1;
    ZoneID_origin = 0; 
    for(i = 0; i<32; ++i){
        LoopEventRAM[i] = 0;
    }
    for(i = 0; i<32; ++i){
        BackboneEventRAM[i] = 0;
    }
    for(i = 0; i<15; ++i){
        RS485EventRAM[i] = 0;
    }  
    for(i = 0; i<32; ++i){
        messageRS485[i] = 0;  
        messageRS485TX[i] = 0;
    }               
    button = 0;
    pushbutton_pressed = 0;
    button_old = 0;
    Event = 1;
    PM_Filter1_timer = 0; 
    PF_Filter2_timer = 0;
    PM_Filter1 = 0;
    PF_Filter2 = 0;
    timeout = 0;
    LearnModeTimer = LearnModeTimerMAX;
    RFtimeout = 0;
    modulatorSIREN = 0;
    RS485_maxADR = EEPROM_read(EEPROM_store+EEnmbRS485);
    RS485_IQnetADR = EEPROM_read(EEPROM_store+EEadrRS485);
    RS485_MessageIndex = 0;
    RS485_MessageRepeat = 0;
    messageLength = 0;
    SPI2RFok = 0;
    SPIRXbufferPointer = 0;   
    for(i = 0;i<32;++i){
        SPI_message2RF[i] = 0;
    }                   
    for(i = 0; i<32; ++i){
        SPI_message2CPU[i] = 0;
    } 
    for(i = 0;i<32;++i){
        SPI_RXbuffer[i] = 0;
    }
    RFmoduleStarting = 0;
    Level3 = 0;
    Programing_Mode = 0; 
    Line1 = 0;
    Line2 = 0;
    ToLevel1Timeout = 0;
    DelayedTransmitionTimeout = 0;
    DelayedTransmitionEN = 0;
    DelayTXminKonst = EEPROM_read(EEPROM_store+EEdelay2TX);
    DelayTXmin = 0;
    DelayTXminKonstQ = 0;
    commFaultFilter1 = EEPROM_read(EEPROM_store+EEcommFaultFilter);
    RecCommBuffer = 0;   
    for(i = 0; i<4; ++i){
        commFaultRegister[i] = 0;
    }
    settingEN54_25 = EEPROM_read(EEPROM_store+EEsettingEN54_25); 
    commTest = 0;
    RS485NewMessage2Send = 0; 
    RS485_Timing = RS485_TimingMin; 
    testRegister = 0;
    maxBackBoneIDdoNotWork = 0;
    MemTestEN = 1;  
    Actual_memory_CRC = 0;
    Original_memory_CRC = EEPROM_read(EEPROM_store+EEoriginalCRC);
    for(i = 0; i<4; ++i){
        ZalohaZ[i] = 0;
    }    
    MemoryTestTimer = 0;     
    CommandExpiration = 0;
    ////////////////////////////////
    RS485_TXEN = 0;
       
    if (DelayTXminKonst > Delay_9min_RFtransmition){
    	DelayTXminKonst = 0;  
        DelayedTransmitionEN = 0xFF;
	}   
                                             
    if (commFaultFilter1 > 0xF0){
        commFaultFilter1 = 1;
        EEPROM_write(EEPROM_store+EEcommFaultFilter,0); 
    }  
    else{
        commFaultFilter1 = commFaultFilter1+1;
    }    
    if (commFaultFilter1 == 1) x = 2;
    else x = commFaultFilter1;
    for(i = 0; i<32; ++i){
        commFaultTimer1[i]=x;    
    }
    for(i = 0; i<8; ++i){                        
        commFaultTimer2[i]=x;    
    }
    
    if (settingEN54_25 > settingEN54_25Konst){
        settingEN54_25 = 1;   
        EEPROM_write(EEPROM_store+EEsettingEN54_25,1);
    }  
                                                              
    DelayTXminKonst = DelayTXminKonst*(Max_Delay_RFtransmition/Delay_RFtransmition);    
    
    if (RS485_maxADR>15){
        RS485_maxADR = 0;
    }
    if (RS485_IQnetADR>15){
        RS485_IQnetADR = 0;
    }
    if ((RS485_maxADR>0)&&(RS485_IQnetADR>0)){
        RS485_IQnet = 1;
    }
     
    RFmoduleStarting = 1; 
    SPI_message2RF[SPI_ID] = 1;
    SPI_message2RF[SPI_SUBLOOP] = 1; 
}

void ResetMessage(void){
unsigned char i;
    //********LOCAL LOOP *******// 
    for(i = 0; i<32; ++i){
        if (BitTest(LoopEventRAM[i],Fire_Alarm_bit)==1){
            BitRST(LoopEventRAM[i],Fire_Alarm_bit);                           
        }  
        if (BitTest(LoopEventRAM[i],Fault_Alarm_bit)==1){
            BitRST(LoopEventRAM[i],Fault_Alarm_bit);                           
        }    
        if (BitTest(LoopEventRAM[i],Battery_Fault_bit)==1){
            BitRST(LoopEventRAM[i],Battery_Fault_bit);                           
        }   
        BitRST(LoopEventRAM[i],Hush_bit);
        BitRST(LoopEventRAM[i],Test_bit); 
    }                             
    Loop_Comm_Fault = 0;
    for(i = 0; i<4; ++i){
        commFaultRegister[i] = 0;
    }   
    for(i = 0; i<32; ++i){
        commFaultTimer1[i]=commFaultFilter1;    
    }
    DelayTXmin = 0;
    DelayedTransmitionTimeout = 0;
    DelayTXminKonstQ = 0;
    //////////////////////////////
    
    
    //********SYSTEM LOOP *******// 
    for(i = 0; i<32; ++i){
        BackboneEventRAM[i] = 0;
    }
    Backbone_Comm_Fault = 0; 
    BackBoneTest = 0xFFFF;          
    for(i = 0; i<8; ++i){                        
        commFaultTimer2[i]=commFaultFilter1;    
    }
    //BitRST(BackBoneTest,(SystemID_origin-1));
    maxBackBoneIDdoNotWork = 0;  
    SystemTestTimer = 1;
    //////////////////////////////
    
                           
    //************RS485**********//
    RS485TestEN = 0;
    RS485_timer = 0; 
    RS485_Comm_Fault = 0;
    RS485NewMessage2Send = 0;
    RS485_MessageIndex = 0; 
    RS485_MessageRepeat = 0;
    RS485Send = 0;
    RS485_Timing = RS485_TimingMin;
    for(i = 0; i<RS485_maxADR; ++i){
        if (BitTest(RS485EventRAM[i],RS485_Fire_Alarm_bit)==1){
            BitRST(RS485EventRAM[i],RS485_Fire_Alarm_bit);
        }
        if (BitTest(RS485EventRAM[i],RS485_Fault_Alarm_bit)==1){
            BitRST(RS485EventRAM[i],RS485_Fault_Alarm_bit);
        } 
        if (BitTest(RS485EventRAM[i],RS485_Bat_Fault_bit)==1){
            BitRST(RS485EventRAM[i],RS485_Bat_Fault_bit);                          
        }
        BitRST(RS485EventRAM[i],RS485_comm_det_bit);    
    }
    RS485_TXEN = 0; 
    gotRS485_message = 0;  
    for(i = 0;i<32;++i){
        messageRS485[i] = 0;
        messageRS485TX[i] = 0;
    }   
    timeout = 0;
    //////////////////////////////          
    
    //***********POWER LINE TEST***********//
    PM_Filter1_timer = 0; 
    PF_Filter2_timer = 0;
    PM_Filter1 = 0;
    PF_Filter2 = 0;
    ////////////////////////////////////////
    //************PANEL SETTINGS**********//
    BitRST(RestartCauses,WDTReset);  
    Actual_memory_CRC = Original_memory_CRC;
    RFmoduleRST = 0;
    blink_LED = 0;
    silence = 0;
    LED_Register = 0;
    Panel_Status = 0;  
    RFmoduleComunication = 0;
    SPIRXbufferPointer = 0; 
    RFtimeout = 0;
    testRegister = 0;
    Programing_Mode = 0;    
    for(i = 0; i<32; ++i){
        SPI_message2CPU[i] = 0;
    }
    for(i = 0;i<32;++i){
        SPI_message2RF[i] = 0;
    }
    for(i = 0;i<32;++i){
        SPI_RXbuffer[i] = 0;
    }  
    for(i = 0;i<4;++i){
        commFaultStored1[i] = 0;
        commFaultStored2[i] = 0;
    }    
    RecCommBuffer = 0;
    MemoryTestTimer = 0;
    SPI_message2RF[SPI_ID] = NodeID_origin;
    SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
    SPI_message2RF[SPI_ZONE] = ZoneID_origin; 
    SPI_message2RF[SPI_COMMAND] = 'N';
    /////////////////////////////////////////
}
void ResetPanelTest(void){
    if ((MCUCSR>>PORF)&0x01 == 1){
        BitSet(RestartCauses,PowerReset);
        BitRST(RestartCauses,UnwantedRestart);
    }                
    else{
        BitRST(RestartCauses,PowerReset);
    }               
    if ((MCUCSR>>BORF)&0x01 == 1){
        BitSet(RestartCauses,BrownReset);
    }                
    else{
        BitRST(RestartCauses,BrownReset);
    }
    if (((MCUCSR>>WDRF)&0x01 == 1)&&(BitTest(RestartCauses,UnwantedRestart)==1)){
        BitSet(RestartCauses,WDTReset);
        #asm("wdr"); 
    }                
    else{
        BitRST(RestartCauses,WDTReset);
    } 
    BitSet(RestartCauses,UnwantedRestart);
    BitSet(RestartCauses,RestartWriteEnable); 
    MCUCSR = 0;
}
void ResetVariables(void){

}
void CPUstartDelay(void){
unsigned char i;
    SS_RF = 1;
    RFprogramng = 0;
    RF_BUTTON = 0;
    /*while (TL < 0x07){
        RF_BUTTON = 1;
        RFprogramng = 1;
    } */
    //RF_BUTTON = 0;
    SS_LED = 0;
    RST_LED = 0; 
    #asm("wdr");
    for(i = 0; i<10;++i){
        delay_msWDR(100);
    } 
    #asm("wdr");  
    for(i = 0;i<10;++i){ 
        delay_msWDR(100);
    }
    #asm("wdr");
}
   
void RFmoduleRestart(void){
unsigned char i;
    SPI_message2RF[SPI_ID] = NodeID_origin;
    SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
    SPI_message2RF[SPI_ZONE] = ZoneID_origin;   
    SPI_message2RF[SPI_COMMAND] = 'X';
    SPI2RFok = 0;   
    while((SPI2RFok != 0x3F)&&(!RFprogramng)){
        SPI2RFok = 0;
        RF_BUTTON=0; 
        SPI2RFok = SPI_Transfer2RF(4);
        RF_BUTTON=1; 
        for(i = 0;i<6;++i){ 
            SPI_Transfer2LED(0xFFFF);
            delay_msWDR(50);           
            SPI_Transfer2LED(0x0000);
            delay_msWDR(50);
        }
    }
    CPUstartDelay(); 
    RFmoduleStarting = 1;
}

void RFmoduleSwitchingON(void){
unsigned char i,y;
    y = 0;  
    while(y < 2){   
        #asm("wdr");
        ++y;
        SPI_message2RF[SPI_ID] = 0;
        SPI_message2RF[SPI_SUBLOOP] = 0;
        SPI_message2RF[SPI_ZONE] = 0;
        SPI_message2RF[SPI_COMMAND] = 0;  
        SPI2RFok = 0;  
        while((SPI2RFok != 0x3F)&&(!RFprogramng)&&(SPI_message2CPU[SPI_COMMAND]!='n')){
            SPI2RFok = 0;
            RF_BUTTON=0; 
            SPI2RFok = SPI_Transfer2RF(4);
            for(i = 0;i<6;++i){ 
                SPI_Transfer2LED(0xFFFF);
                delay_msWDR(50);           
                SPI_Transfer2LED(0x0000);
                delay_msWDR(50);
            }
            RF_BUTTON=1;
            for(i = 0;i<6;++i){ 
                delay_msWDR(100);           
            }
        }
        #asm("wdr");  
        SPI_message2RF[SPI_COMMAND] = '0';
        SPI2RFok = 0;
        while((SPI2RFok != 0x3F)&&(!RFprogramng)){
            SPI2RFok = 0;
            for(i = 0;i<6;++i){ 
                SPI_Transfer2LED(0xFFFF);
                delay_msWDR(50);           
                SPI_Transfer2LED(0x0000);
                delay_msWDR(50);
            }
            RF_BUTTON=1; 
            delay_msWDR(10);
            for(i = 0;i<6;++i){ 
                delay_msWDR(100);           
            } 
            delay_msWDR(10);
            RF_BUTTON=0; 
            SPI2RFok = SPI_Transfer2RF(4);
        }   
        #asm("wdr");
        for(i = 0;i<32;++i){
            SPI_message2RF[i] = 0;
        }   
        SPI_message2RF[0] = 'n';
        SPI2RFok = 0;
        while((SPI2RFok != 0x3F)&&(!RFprogramng)){
            SPI2RFok = 0;
            for(i = 0;i<6;++i){ 
                SPI_Transfer2LED(0xFFFF);
                delay_msWDR(50);           
                SPI_Transfer2LED(0x0000);
                delay_msWDR(50);
            } 
            RF_BUTTON=1; 
            delay_msWDR(10);
            for(i = 0;i<6;++i){ 
                delay_msWDR(100);
            } 
            delay_msWDR(10);
            RF_BUTTON=0; 
            SPI2RFok = SPI_Transfer2RF(32);
        }
        #asm("wdr");
        for(i = 1;i<32;++i){
            SPI_message2RF[i] = SPI_message2CPU[i];
        }   
        SPI_message2RF[0] = 'a';
        SPI2RFok = 0;
        while((SPI2RFok != 0x3F)&&(!RFprogramng)){
            SPI2RFok = 0;
            for(i = 0;i<6;++i){ 
                SPI_Transfer2LED(0xFFFF);
                delay_msWDR(50);           
                SPI_Transfer2LED(0x0000);
                delay_msWDR(50);
            }
            RF_BUTTON=1; 
            delay_msWDR(10);
            for(i = 0;i<6;++i){ 
                delay_msWDR(100);
            } 
            delay_msWDR(10);
            RF_BUTTON=0; 
            SPI2RFok = SPI_Transfer2RF(32);
        }
        #asm("wdr");
        for(i = 1;i<32;++i){
            SPI_message2CPU[i-1] = SPI_message2RF[i];
        }
        for(i = 0;i<32;++i){
            SPI_message2RF[i] = 0;
        }   
        DetNodeNMB = SPI_message2CPU[0];
        if (DetNodeNMB > 0){
            DetNodeNMB = SPI_message2CPU[0];
            //SPI_message2RF[SPI_ID] = SPI_message2CPU[1];
            NodeID_origin = 1;
            BackBoneNMB = SPI_message2CPU[8];                  
            BackBoneTest = 0xFFFF;
            SystemID_origin = SPI_message2CPU[7];
            //BitRST(BackBoneTest,(SPI_message2CPU[SPI_SUBLOOP]-1));
            ZoneID_origin = 0;
            if (BackBoneNMB >= 1){
                RFunprogramed = 0;    
            }
        }
        else{
            NodeID_origin = 1;
            SystemID_origin = 1;
            ZoneID_origin = 0; 
        }
        SPI_message2RF[SPI_ID] = NodeID_origin;
        SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
        SPI_message2RF[SPI_ZONE] = ZoneID_origin; 
        SPI_message2RF[SPI_COMMAND] = 'N'; 
        SPI2RFok = 0;
        while((SPI2RFok != 0x3F)&&(!RFprogramng)){
            SPI2RFok = 0;
            RF_BUTTON=1; 
            delay_msWDR(100);
            RF_BUTTON=0; 
            SPI2RFok = SPI_Transfer2RF(4);
        }
        if (SPI_message2CPU[SPI_COMMAND] == 'a'){
            y = 3;
        }
    }
    #asm("sei")
    SPI_message2RF[SPI_COMMAND] = 'N'; 
    for(i = 0;i<32;++i){
        SPI_RXbuffer[i] = 0;
    }
    SPIRXbufferPointer = 0;    
} 

void SPI_message2CPU_Decoding(void){
unsigned char i,y,x,z,except;
unsigned char bufferTest[4];
//unsigned char message2RF[4]; 
    i = SPI_message2CPU[SPI_ID];
    //if (SPI_message2CPU[SPI_COMMAND]!='n'){         
    //    RS485Message_Construct((SPI_message2CPU[SPI_COMMAND]-0x20));
    //}
    except = 0;
    if (SPI_message2CPU[SPI_SUBLOOP]!=SystemID_origin)
    {
    	except = 1;
    }    
    if ((DelayedTransmitionEN==0xff)||(SPI_message2CPU[SPI_ZONE]==0))
    {
    	except = 1;
    }  
    if (SPI_message2CPU[SPI_COMMAND] != 'n'){                                               
        BitSet(BackBoneTest,(SPI_message2CPU[SPI_SUBLOOP]-1));
    }  
    switch (SPI_message2CPU[SPI_COMMAND]){   
        case 'f':  
            silence = 0;
            if (BitTest(testRegister,TestMode)==0){
                if (SPI_message2CPU[SPI_SUBLOOP]==SystemID_origin){
                    if (BitTest(LoopEventRAM[i],Fire_Alarm_bit)==0){
                        RFEventSave(SPI_message2CPU[SPI_ID],SPI_message2CPU[SPI_SUBLOOP],SPI_message2CPU[SPI_ZONE],SPI_message2CPU[SPI_COMMAND]);
                    }
                }
                else if (BitTest(BackboneEventRAM[SPI_message2CPU[SPI_SUBLOOP]],Fire_Alarm_bit)==0){
                    RFEventSave(SPI_message2CPU[SPI_ID],SPI_message2CPU[SPI_SUBLOOP],SPI_message2CPU[SPI_ZONE],SPI_message2CPU[SPI_COMMAND]);
                }
                DelayTXmin = 0; 
                RS485Message_Construct(SPI_message2CPU[SPI_ID],SPI_message2CPU[SPI_SUBLOOP],SPI_message2CPU[SPI_ZONE],(SPI_message2CPU[SPI_COMMAND]-0x20));
            }
            else{
                except = 1;
            }
            if (SPI_message2CPU[SPI_SUBLOOP]==SystemID_origin){
                BitSet(LoopEventRAM[i],Fire_Alarm_bit);
            }
            else{ 
                if (SPI_message2CPU[SPI_ZONE]==0){
                    BitSet(BackboneEventRAM[SPI_message2CPU[SPI_SUBLOOP]],Fire_Alarm_bit);
                }
            }   
            BackBoneTest = 0xFFFF;                  
            Clock3 = 0;   
        break;
        case 'i':
            if ((SPI_message2CPU[SPI_SUBLOOP]==SystemID_origin)&&(SPI_message2CPU[SPI_ID]==NodeID_origin)){  
                RFEventHistoryOUT();
            }
            except = 1;
        break;
        case 's':
            if (SPI_message2CPU[SPI_SUBLOOP]!=SystemID_origin){
                BitRST(BackboneEventRAM[SPI_message2CPU[SPI_SUBLOOP]],Fire_Alarm_bit); 
                CLR_INT_SIREN_SW; 
            }
            if (BitTest(testRegister,TestMode)==0){
                DelayTXmin = DelayTXminKonst;
                RS485Message_Construct(SPI_message2CPU[SPI_ID],SPI_message2CPU[SPI_SUBLOOP],SPI_message2CPU[SPI_ZONE],(SPI_message2CPU[SPI_COMMAND]-0x20));
            }
            else{
                except = 1;
            } 
            BackBoneTest = 0xFFFF;
            Clock3 = 0;                
        break;
        case 'q':
            if ((BitTest(BackboneEventRAM[SPI_message2CPU[SPI_SUBLOOP]],Fire_Alarm_bit)==1)||(BitTest(BackboneEventRAM[SPI_message2CPU[SPI_SUBLOOP]],Fault_Alarm_bit)==1)){
                CLR_INT_SIREN_SW;  
                silence = 1;
            }                   
            y = 0;       
            if (BitTest(BackboneEventRAM[SPI_message2CPU[SPI_SUBLOOP]],Fire_Alarm_bit)==1){
                y = 1;
            }                   
            if (SPI_message2CPU[SPI_SUBLOOP]==SystemID_origin){
                BitRST(LoopEventRAM[i],Fault_Alarm_bit);                        
            }
            else{ 
                BitRST(BackboneEventRAM[SPI_message2CPU[SPI_SUBLOOP]],Fault_Alarm_bit);
            }    
            if (BitTest(testRegister,TestMode)==0){
                RS485Message_Construct(SPI_message2CPU[SPI_ID],SPI_message2CPU[SPI_SUBLOOP],SPI_message2CPU[SPI_ZONE],(SPI_message2CPU[SPI_COMMAND]-0x20));
            }
            if (y == 1){ 
                SPI_message2CPU[SPI_ID] = NodeID_origin;
                SPI_message2CPU[SPI_SUBLOOP] = SystemID_origin;
                SPI_message2CPU[SPI_ZONE] = ZoneID_origin; 
                DelayedTransmitionEN = 0; 
                DelayTXminKonstQ = Delay_RFtransmition;                                                             
                DelayTXmin = DelayTXminKonstQ-SystemID_origin;
                except = 0;  
            }             
            else{
                except = 1;
            } 
            BackBoneTest = 0xFFFF;
            Clock3 = 0;
        break;
        case 'h':
            if (SPI_message2CPU[SPI_SUBLOOP]==SystemID_origin){        
                BitSet(LoopEventRAM[i],Hush_bit);                              
            }   
            else{
                BitSet(BackboneEventRAM[SPI_message2CPU[SPI_SUBLOOP]],Hush_bit);
            } 
            if (BitTest(testRegister,TestMode)==0){
                RS485Message_Construct(SPI_message2CPU[SPI_ID],SPI_message2CPU[SPI_SUBLOOP],SPI_message2CPU[SPI_ZONE],(SPI_message2CPU[SPI_COMMAND]-0x20));
            }
            else{
                except = 1;
            }
            Clock3 = 0;
        break;
        case 'e':
            silence = 0;
            if (BitTest(testRegister,TestMode)==0){
                RS485Message_Construct(SPI_message2CPU[SPI_ID],SPI_message2CPU[SPI_SUBLOOP],SPI_message2CPU[SPI_ZONE],(SPI_message2CPU[SPI_COMMAND]-0x20));
                if (SPI_message2CPU[SPI_SUBLOOP]==SystemID_origin){
                    if (BitTest(LoopEventRAM[i],Fault_Alarm_bit)==0){  
                        RFEventSave(SPI_message2CPU[SPI_ID],SPI_message2CPU[SPI_SUBLOOP],SPI_message2CPU[SPI_ZONE],SPI_message2CPU[SPI_COMMAND]);
                    }                    
                }
                else if (BitTest(BackboneEventRAM[SPI_message2CPU[SPI_SUBLOOP]],Fault_Alarm_bit)==0){  
                    RFEventSave(SPI_message2CPU[SPI_ID],SPI_message2CPU[SPI_SUBLOOP],SPI_message2CPU[SPI_ZONE],SPI_message2CPU[SPI_COMMAND]);
                } 
            }
            if (SPI_message2CPU[SPI_SUBLOOP]==SystemID_origin){
                BitSet(LoopEventRAM[i],Fault_Alarm_bit);                       
            }
            else{
                BitSet(BackboneEventRAM[SPI_message2CPU[SPI_SUBLOOP]],Fault_Alarm_bit);
            } 
            Clock3 = 0;    
            except = 1;
        break;  
        case 't':     
            if (RFunprogramed==1){
                BitSet(LoopEventRAM[i],Fire_Alarm_bit);
                BitSet(BackboneEventRAM[i],Fire_Alarm_bit);
            }
            else{
                if (SPI_message2CPU[SPI_SUBLOOP]==SystemID_origin){
                    BitSet(LoopEventRAM[i],Test_bit);                             
                }
                else{
                    BitSet(BackboneEventRAM[SPI_message2CPU[SPI_SUBLOOP]],Test_bit);
                }    
            }  
            if (BitTest(testRegister,TestMode)==0){  
                RS485Message_Construct(SPI_message2CPU[SPI_ID],SPI_message2CPU[SPI_SUBLOOP],SPI_message2CPU[SPI_ZONE],(SPI_message2CPU[SPI_COMMAND]-0x20));
            }
            Clock3 = 0;
            except = 1; 
        break;
        case 'd':
            RFmoduleRST = 1; 
            except = 1;
        break; 
        case 'l':
            if (SPI_message2CPU[SPI_SUBLOOP]==0){
                BitSet(LearnStatus,BackboneLearnMode);
            }
            if (BitTest(LearnStatus,RemoteLearnMode) == 1){
                DoLearnReset = 1;  
                LearnStatus = 0;      
            }                      
            else{
                BitSet(LearnStatus,RemoteLearnMode);
            } 
            except = 1;
        break;
        case 'k': 
            RS485Send = 0; 
            #asm("wdr")
            if (SPI_message2CPU[SPI_SUBLOOP]==SystemID_origin){ 
                if (SPI_message2CPU[SPI_ID]==1){
                    RF_BUTTON=1;
                    delay_msWDR(62);
                    RecCommBuffer = 1;     
                    SPI2RFok = 0;                     
                    z = 0;  
                    while((SPI2RFok != 0x3F)&&(z<10)){
                        ++z;
                        SPI2RFok = 0; 
                        SPI_message2RF[SPI_ID] = NodeID_origin;
                        SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
                        SPI_message2RF[SPI_ZONE] = ZoneID_origin;  
                        SPI_message2RF[SPI_COMMAND] = '4';
                        SPI2RFok = SPI_Transfer2RF(4);
                    }                            
                    RF_BUTTON=1; 
                }
                if(z<10){                  
                    z = 0;
                    delay_msWDR(20);
                    SPI2RFok = 0;        
                    SPI2RFok = SPI_Transfer2RF(9);
                    if (SPI2RFok == 0x3F){
                        if (SPI_message2CPU[SPI_COMMAND] == '4'){
                            for (i = 0;i < 4;++i){
                                commFaultRegister[i] = SPI_message2CPU[SPI_COMMAND+(i+1)];                
                            }
                            BackBoneTest = SPI_message2CPU[SPI_COMMAND+5];
                            //****************Local TEST************************//
                            for(y=0;y<32;++y){          
                                //go over the LoopEventRAM and if there is alarm state fill all commFaultRegister bits with log.1                                                      
                                if (BitTest(LoopEventRAM[y],Fire_Alarm_bit)==1){
                                   for (i=0;i<4;++i){
                                       commFaultRegister[i] = 0xFF;     
                                   }
                                   break;    
                                }                                                                                                                        
                            }         
                            //check commFaultRefister and if bit is 0, decrement timer. If bit is 1, set timer to filter value               
                            i = 0;
                            x = commFaultRegister[i];
                            for(y=0;y<DetNodeNMB;++y){ 
                                if ((x&0x01) == 0){     
                                    if (commFaultTimer1[y]>0){
                                        --commFaultTimer1[y];     
                                    }                                                  
                                }
                                else{
                                    commFaultTimer1[y] = commFaultFilter1;   
                                } 
                                if ((y>0)&&((y+1)%8==0)){
                                    ++i;
                                    x = commFaultRegister[i];           
                                }
                                else{
                                    x = (x>>1);
                                }
                            }      
                            i = 0;    
                            x = commFaultStored1[i];
                            //if commFaultFilter1 > 10 the comm fault indication turned off                                                            
                            if (commFaultFilter1 <= MAXcommFaultFilter){
                                //check all timers and if is 0, the commfault is indicated.                       
                                Loop_Comm_Fault = 0; 
                                for(y=0;y<32;++y){               
                                    if ((commFaultTimer1[y] < commFaultFilter1)&&(settingEN54_25 == 1)){
                                        Loop_Comm_Fault = 1;
                                    }   
                                    z = y%8;
                                    if (commFaultTimer1[y] == 0){
                                        Loop_Comm_Fault = 1;                                          
                                        if (BitTest(x,z)==0){
                                           RFEventSave(y+1,SystemID_origin,0,'k');
                                           BitSet(commFaultStored1[i],z);
                                        }
                                    }
                                    else{
                                        BitRST(commFaultStored1[i],z);
                                    }
                                    if ((y>0)&&((y+1)%8==0)){
                                        ++i;
                                        x = commFaultStored1[i]; 
                                        #asm("wdr")        
                                    }
                                }
                            }                                  
                            //siren is beeping caused by comm fault stop it
                            if ((x == 1)&&(Loop_Comm_Fault == 0)){
                               silence = 0;
                            }
                            //--------------------Local TEST-------------------//
 
                            //*******************System TEST*******************//
                            for(y=0;y<8;++y){                                                       
                                if (BitTest(BackboneEventRAM[y],Fire_Alarm_bit)==1){
                                    BitSet(BackBoneTest,y);    
                                }                  
                            }
                            
                            //check BackBoneTest and if bit is 0, decrement timer. If bit is 1, set timer to filter value               
                            for(y=0;y<BackBoneNMB;++y){
                                if (BitTest(BackBoneTest,y)==0){     
                                    if (commFaultTimer2[y]>0){
                                        --commFaultTimer2[y];     
                                    }                                                  
                                }
                                else{
                                    commFaultTimer2[y] = commFaultFilter1;   
                                } 
                            }
                            i = 0;    
                            x = commFaultStored2[i];
                            //if commFaultFilter1 > 10 the comm fault indication turned off                                                            
                            if (commFaultFilter1 <= MAXcommFaultFilter){
                                //check all timers and if is 0, the commfault is indicated.                            
                                Backbone_Comm_Fault = 0; 
                                #asm("wdr")
                                for(y=0;y<8;++y){               
                                    if ((commFaultTimer2[y] < commFaultFilter1)&&(settingEN54_25 == 1)){
                                        Backbone_Comm_Fault = 1;
                                    }      
                                    z = y%8;
                                    if (commFaultTimer2[y] == 0){
                                        Backbone_Comm_Fault = 1;  
                                        if (BitTest(x,z)==0){
                                            RFEventSave(1,y+1,0,'k'); 
                                            BitSet(commFaultStored2[i],z);
                                        }
                                    }
                                    else{
                                        BitRST(commFaultStored2[i],z);
                                    }
                                    /*if ((y>0)&&((y+1)%8==0)){
                                        ++i;
                                        x = commFaultStored2[i];         
                                    }*/
                                }
                            }
                            //--------------------System TEST-------------------// 
                        }
                    }
                }        
            }                                                  
            except = 1;
        break;
        case 'b':                                              
            silence = 0;
            if (SPI_message2CPU[SPI_SUBLOOP]==SystemID_origin){
                BitSet(LoopEventRAM[i],Battery_Fault_bit);
            }
            else{
                BitSet(BackboneEventRAM[SPI_message2CPU[SPI_SUBLOOP]],Battery_Fault_bit);
            }  
            if (BitTest(testRegister,TestMode)==0){  
                RFEventSave(SPI_message2CPU[SPI_ID],SPI_message2CPU[SPI_SUBLOOP],SPI_message2CPU[SPI_ZONE],SPI_message2CPU[SPI_COMMAND]);
                RS485Message_Construct(SPI_message2CPU[SPI_ID],SPI_message2CPU[SPI_SUBLOOP],SPI_message2CPU[SPI_ZONE],(SPI_message2CPU[SPI_COMMAND]-0x20));
            }
            Clock3 = 0;
            except = 1;  
        break;
        case 'c': 
            if (SPI_message2CPU[SPI_SUBLOOP]==SystemID_origin){
                BitRST(LoopEventRAM[i],Fault_Alarm_bit);
                BitRST(LoopEventRAM[i],Battery_Fault_bit);                    
            }
            else{
                BitRST(BackboneEventRAM[SPI_message2CPU[SPI_SUBLOOP]],Fault_Alarm_bit);
                BitRST(BackboneEventRAM[SPI_message2CPU[SPI_SUBLOOP]],Battery_Fault_bit);
            }
            /*if (SPI_message2CPU[SPI_SUBLOOP]==SystemID_origin){   
                Loop_Comm_Fault = 0;
                SaveTestBuffer(0,0,0,0);
                commFaultFilter = EEPROM_read(EEPROM_store+EEcommFaultFilter);
                for(i = 0; i<32; ++i){
                    //LoopEventRAM[i] = 0;
                    BitRST(LoopEventRAM[i],Battery_Fault_bit); 
                    BitRST(LoopEventRAM[i],Fault_Alarm_bit);
                }                        
            }   
            else{
                Backbone_Comm_Fault = 0;                      
                BitRST(BackboneEventRAM[i],Battery_Fault_bit);
                BitRST(BackboneEventRAM[i],Fault_Alarm_bit);
            }  */
            if (BitTest(testRegister,TestMode)==0){
                RS485Message_Construct(SPI_message2CPU[SPI_ID],SPI_message2CPU[SPI_SUBLOOP],SPI_message2CPU[SPI_ZONE],(SPI_message2CPU[SPI_COMMAND]-0x20));
            }
            except = 1;      
        break;  
        case 'm': 
            if (SPI_message2CPU[SPI_SUBLOOP]!=SystemID_origin){
                Backbone_Comm_Fault = 0;  
            }
            /*if (SPI_message2CPU[SPI_SUBLOOP]==SystemID_origin){
                Loop_Comm_Fault = 0; 
                for(y = 0;y < 4;++y){ 
                    commFaultRegister[y] = 0;
                }              
            }
            else{
                Backbone_Comm_Fault = 0;  
            } */
            if (BitTest(testRegister,TestMode)==0){
                RS485Message_Construct(SPI_message2CPU[SPI_ID],SPI_message2CPU[SPI_SUBLOOP],SPI_message2CPU[SPI_ZONE],(SPI_message2CPU[SPI_COMMAND]-0x20));
            }
            except = 1;      
        break;
        case 'r':
            //if ((BitTest(Panel_Status,L_ALARM_bit)==0)||(SPI_message2CPU[SPI_SUBLOOP]==SPI_message2RF[SPI_SUBLOOP])){
            if (BitTest(testRegister,TestMode)==0){
                RS485Message_Construct(SPI_message2CPU[SPI_ID],SPI_message2CPU[SPI_SUBLOOP],SPI_message2CPU[SPI_ZONE],(SPI_message2CPU[SPI_COMMAND]-0x20));
            } 
            if (settingEN54_25 == 0){
                if ((SPI_message2CPU[SPI_SUBLOOP]==SPI_message2RF[SPI_SUBLOOP])&&(RFmoduleRST == 0)){                                     
                    //if (RFmoduleRST == 0){
                        ResetMessage();
                        RFmoduleRST = 1;
                    //}
                    //RFmoduleRestart();
                } 
            }
            except = 1;
        break;
        case 'w':     
        	NEG_INT_SIREN_SW;
            if ((SPI_message2CPU[SPI_SUBLOOP]==SystemID_origin)&&(SPI_message2CPU[SPI_ID]==NodeID_origin)){
            	
                delay_msWDR(20); 
                for(i = 0;i<8;++i){
                	SPI_message2CPU[i] = 0;
                }
                SPI2RFok = 0; 
                while(SPI2RFok != 0x3F){
                 	RF_BUTTON=0;
                    SPI2RFok = 0; 
                    SPI2RFok = SPI_Transfer2RF(8);                      
                	delay_msWDR(50);           
                    RF_BUTTON=1;
            		delay_msWDR(100);                              
                }
                SPI_Transfer2LED(0xFFFF); 
                delay_msWDR(100);
                delay_msWDR(100);              
                /*for(i = 0;i<6;++i){ 
                    SPI_Transfer2LED(0xFFFF);
                    delay_msWDR(50);           
                    SPI_Transfer2LED(0x0000);
                    delay_msWDR(50);
                }  */                                     
                //i = (180/Delay_RFtransmition);                                      
                //SPI_message2CPU[0] = SPI_message2CPU[0]/i;
                if (SPI_message2CPU[0]<=(Delay_9min_RFtransmition+1)){
                    EEPROM_write(EEPROM_store+EEdelay2TX,SPI_message2CPU[0]);
                }                            
                if (SPI_message2CPU[1]<=MAXcommFaultFilter){
                    EEPROM_write(EEPROM_store+EEcommFaultFilter,SPI_message2CPU[1]);
                }
                if (SPI_message2CPU[2]<=settingEN54_25Konst){
                    EEPROM_write(EEPROM_store+EEsettingEN54_25,SPI_message2CPU[2]);
                }
                delay_msWDR(100); 
                NEG_INT_SIREN_SW;               
                BitRST(RestartCauses,UnwantedRestart);
                #pragma optsize-
                WDTCR=0x18;
                WDTCR=0x08;
                #ifdef _OPTIMIZE_SIZE_
                #pragma optsize+
                #endif
                #asm("wdr")
                while(1);                       
            }        
        break;  
        case '0':
            SPI_Transfer2LED(0x5555); 
            BitRST(RestartCauses,UnwantedRestart);                                
            #pragma optsize-
            WDTCR=0x18;
            WDTCR=0x08;
            #ifdef _OPTIMIZE_SIZE_
            #pragma optsize+
            #endif
            #asm("wdr")
            while(1); 
        break;
        case 'p':
        	except = 1;  
            Programing_Mode = 1;
        break;
        case '1':      
        	except = 1;
            Programing_Mode = 2;
        break; 
        case '2':      
        	except = 1;
            Programing_Mode = 3;
        break;
        case '3':      
        	except = 1;
            Programing_Mode = 4;
        break;
        case '7':      
        	except = 1;   
            ++commTest;  
            if (commTest == 200){
                for(i = 0; i<10; ++i){
                    SPI_Transfer2LED(0xFFFF); 
                    delay_ms(100);
                    SPI_Transfer2LED(0x0000);
                    delay_ms(100);
                }                    
            }
        break;  
        case 'n':      
        	except = 1;     
            if (RFmoduleStarting>0){
                CLR_INT_SIREN_SW;
                RFmoduleSwitchingON();   
                RFmoduleStarting = 0; 
            }
        break;
        default:
        	except = 1;
        break;
    }     
    if (SPI_message2CPU[SPI_COMMAND] != 'n'){
        Event = 1;  
    }
    if (except == 0){
        z = 0;  
        x = 0;
        for(i = 0;i<SPIRXbufferPointer;i=i+4){
        	y = 0;  
            z = 0;
            while(y < 4){
        		bufferTest[y] = SPI_RXbuffer[i+y];
            	++y;
            }                  
            if (SPI_message2CPU[SPI_ID]==bufferTest[SPI_ID]){
                z = 0x01; 
            }
            if (SPI_message2CPU[SPI_SUBLOOP]==bufferTest[SPI_SUBLOOP]){
                z = z|0x02; 
            }
            if (SPI_message2CPU[SPI_ZONE]==bufferTest[SPI_ZONE]){
                z = z|0x04; 
            }            
            if (SPI_message2CPU[SPI_COMMAND]==bufferTest[SPI_COMMAND]){
                z = z|0x08; 
            }
            if (z == 15){
                x = 2;
            	break;
            }            
            else if(SPI_message2CPU[SPI_COMMAND]=='h'){
            	z = (z&0x06);
            	if(z == 0x06){
                    x = 1; 
                    y = i;
                    break;
                } 
            }
            else if(SPI_message2CPU[SPI_COMMAND]=='f'){
            	z = (z&0x02);
            	if(z == 0x02){
                    x = 2; 
                    break;
                }  
            }
            else if(SPI_message2CPU[SPI_COMMAND]=='s'){
            	z = (z&0x07);
            	if(z == 0x07){
                    x = 1; 
                    y = i;
                    break;
                }  
            }
        }
        if (x == 0){
        	//SPI_message2CPU[SPI_ZONE] = 0; 
            for(i = 0;i<4;++i){
                SPI_RXbuffer[i+SPIRXbufferPointer] = SPI_message2CPU[i];
            }                                                           
            SPIRXbufferPointer += 4;
        }
        else if(x == 1){
		    for(i = y;i<SPIRXbufferPointer;++i){
                if	((i+4)<32){
                	SPI_RXbuffer[i] = SPI_RXbuffer[i+4];
                }   
                else{
                	SPI_RXbuffer[i] = 0;
                }
            } 
            for(i = SPIRXbufferPointer-4;i<SPIRXbufferPointer;++i){
            	SPI_RXbuffer[i] = 0;
            }
            SPIRXbufferPointer -= 4;
        } 
        //EEPROM_write(0,SPIRXbufferPointer);               
    }
}

unsigned char TestEvent2Fix(unsigned char T_ID,unsigned char T_SUB,unsigned char T_ZONE,unsigned char T_COMMAND){
unsigned char i,y,z;
    i = 0;              
    z = commFaultNodeMem[0]^T_ID;
    for (y = 0;y<8;++y){
        if ((z&0x01) == 1){
            if ((T_ID&0x01) == 1){
                ++i;
            }
        }
        T_ID = (T_ID>>1); 
        z = (z>>1);
    }                            
    z = commFaultNodeMem[1]^T_SUB;
    for (y = 0;y<8;++y){
        if ((z&0x01) == 1){
            if ((T_SUB&0x01) == 1){
                ++i;
            }
        }
        T_SUB = (T_SUB>>1); 
        z = (z>>1);
    } 
    z = commFaultNodeMem[2]^T_ZONE;
    for (y = 0;y<8;++y){
        if ((z&0x01) == 1){
            if ((T_ZONE&0x01) == 1){
                ++i;
            }
        }
        T_ZONE = (T_ZONE>>1); 
        z = (z>>1);
    } 
    z = commFaultNodeMem[3]^T_COMMAND;
    for (y = 0;y<8;++y){
        if ((z&0x01) == 1){
            if ((T_COMMAND&0x01) == 1){
                ++i;
            }
        }
        T_COMMAND = (T_COMMAND>>1); 
        z = (z>>1);
    }                      
    if (i > 0){
        return(1);
    }             
    return(0);
}

unsigned char TestEvent1Fix(unsigned char T_ID){
unsigned char i,y,z;
    i = 0;              
    z = commFaultSysMem^T_ID;
    for (y = 0;y<8;++y){
        if ((z&0x01) == 1){
            if ((T_ID&0x01) == 1){
                ++i;
            }
        }
        T_ID = (T_ID>>1); 
        z = (z>>1);
    }                                                  
    if (i > 0){
        return(1);
    }             
    return(0);
}
                   
void RFEventHistoryOUT(void){ //Time 1200ms
unsigned char message2RF[5],a,i;
unsigned int addr,b;
unsigned char sett,comm,del;
    message2RF[0] = SPI_message2RF[0];
    message2RF[1] = SPI_message2RF[1];
    message2RF[2] = SPI_message2RF[2];
    message2RF[3] = SPI_message2RF[3];
    message2RF[4] = SPI_message2RF[4];    
    //SPI_message2RF[SPI_COMMAND] = 'I';
    //SPI_message2RF[SPI_ZONE] = BackBoneTest; 
    //SPI_message2RF[SPI_SUBLOOP] = BackBoneNMB;
    //SPI_message2RF[SPI_ZONE] = Line1; 
    //SPI_message2RF[SPI_SUBLOOP] = Line2;
    addr = 0;
    addr = EEPROM_read(EEPROM_store+EEadrHRFEvent)<<8;    
    addr = addr + EEPROM_read(EEPROM_store+EEadrLRFEvent);
    if (addr > 0x01DB){
        addr = 0;
    } 
    for (i = 0;i<32;++i){
        SPI_message2RF[i] = 0;
    }           
    b = 0;  
    del = EEPROM_read(EEPROM_store+EEdelay2TX);
    comm = EEPROM_read(EEPROM_store+EEcommFaultFilter);
    sett = EEPROM_read(EEPROM_store+EEsettingEN54_25);
    a = addr/30;
    while(a>0){
        SPI2RFok = 0;    
        SPI_message2RF[SPI_ID] = del;
        SPI_message2RF[SPI_SUBLOOP] = comm+9;
        SPI_message2RF[SPI_ZONE] = sett;
        SPI_message2RF[SPI_COMMAND] = 'I';
        while((SPI2RFok != 0x3F)&&(!RFprogramng)){
            SPI2RFok = 0;
            RF_BUTTON=0; 
            SPI2RFok = SPI_Transfer2RF(4);
            RF_BUTTON=1;      
            #asm("wdr");
            delay_msWDR(200);
            delay_msWDR(200);
        }
        for (i = 0;i<30;++i){
            SPI_message2RF[i] = EEPROM_read(i + EEPROMRFEventDATA + b);
        }
        --a;
        b = b+30;        
        SPI2RFok = 0;
        while((SPI2RFok != 0x3F)&&(!RFprogramng)){
            SPI2RFok = 0;
            RF_BUTTON=0; 
            SPI2RFok = SPI_Transfer2RF(30);
            RF_BUTTON=1; 
            //delay_msWDR(200);
            delay_msWDR(200);
        }            
    }
    SPI2RFok = 0;
    SPI_message2RF[SPI_ID] = del;
    SPI_message2RF[SPI_SUBLOOP] = comm+9;
    SPI_message2RF[SPI_ZONE] = sett; 
    SPI_message2RF[SPI_COMMAND] = 'I';
    while((SPI2RFok != 0x3F)&&(!RFprogramng)){
        SPI2RFok = 0;
        RF_BUTTON=0; 
        SPI2RFok = SPI_Transfer2RF(4);
        RF_BUTTON=1;      
        #asm("wdr");
        delay_msWDR(200);
        delay_msWDR(200);
    }           
    for (i = 0;i<30;++i){
        SPI_message2RF[i] = 0;
    }
    SPI2RFok = 0;
    a = addr%30;
    if (a >= 0){
        for (i = 0;i<a;++i){
            SPI_message2RF[i] = EEPROM_read(i + EEPROMRFEventDATA + b);
        }
        while((SPI2RFok != 0x3F)&&(!RFprogramng)){
            SPI2RFok = 0;
            RF_BUTTON=0; 
            SPI2RFok = SPI_Transfer2RF(30);
            RF_BUTTON=1; 
            //delay_msWDR(200);
            delay_msWDR(200);
        }          
    }
    SPI_message2RF[0] = message2RF[0];
    SPI_message2RF[1] = message2RF[1];
    SPI_message2RF[2] = message2RF[2];
    SPI_message2RF[3] = message2RF[3];
    SPI_message2RF[4] = message2RF[4];
}

unsigned int CheckEventStatus(unsigned int data){
unsigned char i;                          
    BitRST(data,L_ALARM_bit);
    BitRST(data,R_ALARM_bit);
    BitRST(data,FAULT_bit);
    BitRST(data,TESTMODE_bit);        
    BitRST(data,L_BATTERY_bit);
    BitRST(data,TEST_bit); 
    BitRST(data,HUSH_bit);
    for(i=0;i<32;++i){               
        if (BitTest(LoopEventRAM[i],Fire_Alarm_bit)==1){
            BitSet(data,L_ALARM_bit);
        }                                           
        if (BitTest(BackboneEventRAM[i],Fire_Alarm_bit)==1){
            BitSet(data,R_ALARM_bit);
        }
        if (BitTest(LoopEventRAM[i],Fault_Alarm_bit)==1){
            BitSet(data,FAULT_bit);
        }
        if (BitTest(LoopEventRAM[i],Battery_Fault_bit)==1){
            BitSet(data,L_BATTERY_bit);
        } 
        /*if (BitTest(BackboneEventRAM[i],Battery_Fault_bit)==1){
            BitSet(data,L_BATTERY_bit);
        }*/ 
        if (BitTest(LoopEventRAM[i],Test_bit)==1){
            BitSet(data,TEST_bit);
            if (Clock3>=TESTtimeout){
                 BitRST(LoopEventRAM[i],Test_bit);
                 CLR_INT_SIREN_SW;  
            }            
        } 
        if (BitTest(LoopEventRAM[i],Hush_bit)==1){
            BitSet(data,HUSH_bit);
            if (Clock3>=HUSHtimeout){
                 BitRST(LoopEventRAM[i],Hush_bit);
                 CLR_INT_SIREN_SW;  
            }              
        }                   
    } 
    for(i = 0; i<RS485_maxADR; ++i){
        if (BitTest(RS485EventRAM[i],Fire_Alarm_bit)==1){
            BitSet(data,R_ALARM_bit);
        }
        if (BitTest(RS485EventRAM[i],RS485_Fault_Alarm_bit)==1){
            BitSet(data,FAULT_bit);
        }
    }                        
    if (BitTest(LoopEventRAM[NodeID_origin],Power_Fault_bit)==1){
        BitSet(data,P_FAULT_bit);
    }                            
    else{
        BitRST(data,P_FAULT_bit);
    }
    /*if (Power == 1){
        BitSet(data,POWER_bit);   
    }     
    else{
        BitRST(data,POWER_bit);
    }*/
    if (Loop_Comm_Fault==1){
        BitSet(data,RF_L_FAULT_bit);              
    }                               
    else{
        BitRST(data,RF_L_FAULT_bit);
    }
    if (Backbone_Comm_Fault==1){
        BitSet(data,RF_B_FAULT_bit);              
    }
    else{
        BitRST(data,RF_B_FAULT_bit);
    }                  
    if (RS485_Comm_Fault==1){
        BitSet(data,RS485_FAULT_bit);              
    }         
    else{
        BitRST(data,RS485_FAULT_bit);    
    }
    
    if (BitTest(testRegister,TestMode)==1){
        BitSet(data,TESTMODE_bit);               
    }               
    
    /*if (BitTest(RestartCauses,PowerReset)==1){
        BitSet(data,R_ALARM_bit);               
    } 
    if (BitTest(RestartCauses,BrownReset)==1){
        BitSet(data,TEST_bit);               
    }*/
    if (BitTest(RestartCauses,WDTReset)==1){
        BitSet(data,SYS_FAULT_bit);              
        if (BitTest(RestartCauses,RestartWriteEnable)==1){ 
            RFEventSave(NodeID_origin,SystemID_origin,ZoneID_origin,'1'); 
        }
        BitRST(RestartCauses,RestartWriteEnable);              
    }
    else if (Actual_memory_CRC != Original_memory_CRC){
        BitSet(data,SYS_FAULT_bit);                                                                                              
        RFEventSave(NodeID_origin,SystemID_origin,ZoneID_origin,'2');
    }
    else if (RFmoduleComunication == 1){
        /*if (RFtimeout >= RFmoduleBusyKonst){
            RFmoduleBusy = 1;                
        } */
        if (RFtimeout >= RFtimeoutKonst){                                                                                        
            BitSet(data,SYS_FAULT_bit); 
            silence = 0;                                                                                                         
            RFEventSave(NodeID_origin,SystemID_origin,ZoneID_origin,'3');
            //RFmoduleBusy = 0; 
        }
    }
    else{                                          
        BitRST(data,SYS_FAULT_bit); 
        //RFmoduleBusy = 0;
    } 
    if (RFmoduleRST == 1){
        if (RFtimeout >= RFresetKonst){
           RFmoduleRST = 0;
           RFtimeout = 0;
        }
    }
    /*if (SPI2RFok == 0xFF){
        BitSet(data,RF_L_FAULT_LED); 
        BitSet(data,RF_B_FAULT_bit);
    }
    else{
        BitRST(data,RF_L_FAULT_LED); 
        BitRST(data,RF_B_FAULT_bit);
    } */     
    return(data);
}

unsigned int Indication(unsigned int input){
unsigned int data;
    data = 0;  
    if ((RFunprogramed==1)&&(ProductionMode==0)){
        if ((Clock2%ModulationLED)==0){ 
            BitSet(data,FAULT_LED);
            BitSet(data,RF_L_FAULT_LED);  
            BitSet(data,RF_B_FAULT_LED);             
        }   
        else{
            BitRST(data,FAULT_LED);
            BitRST(data,RF_L_FAULT_LED);
            BitRST(data,RF_B_FAULT_LED);   
        } 
    }
    if (Programing_Mode == 1){         
        BitSet(data,BATTERY_LED);
        //BitSet(data,DISABLE_LED);
        if ((Clock2%ModulationLED)==0){
            BitSet(data,R_ALARM_LED);  
            //BitSet(data,RF_B_FAULT_LED);
            //BitSet(data,RF_L_FAULT_LED);             
        }   
        else{  
            BitRST(data,R_ALARM_LED);
            BitRST(data,RF_L_FAULT_LED);
            BitRST(data,RF_B_FAULT_LED);   
        }
    } 
    if (Programing_Mode == 2){
        //BitSet(data,RF_B_FAULT_LED);
        //BitSet(data,RF_L_FAULT_LED);         
        //BitSet(data,DISABLE_LED);
        BitSet(data,R_ALARM_LED); 
        if ((Clock2%ModulationLED)==0){  
            //BitSet(data,DISABLE_LED);
            BitSet(data,BATTERY_LED);             
        }   
        else{ 
            //BitRST(data,DISABLE_LED);
            BitRST(data,BATTERY_LED);   
        }
    } 
                                  
    if ((BitTest(LearnStatus,LearnMode) == 1)||(BitTest(LearnStatus,RemoteLearnMode) == 1)){
           switch (Programing_Mode)
           {
           case 3:
                BitSet(data,DISABLE_LED);
                if (BitTest(LearnStatus,BackboneLearnMode) == 1){
                    BitSet(data,RF_B_FAULT_LED);        
                }
                else{
                    BitSet(data,RF_L_FAULT_LED);
                }
                if ((Clock2%ModulationLED)==0){
                //    BitRST(data,DISABLE_LED);
                    BitRST(data,RF_L_FAULT_LED);  
                    BitRST(data,RF_B_FAULT_LED);             
                }
           break;
           case 4:
                BitSet(data,DISABLE_LED);         
                BitRST(data,RF_L_FAULT_LED);  
                BitRST(data,RF_B_FAULT_LED);
                if (BitTest(LearnStatus,BackboneLearnMode) == 1){
                    BitSet(data,RF_B_FAULT_LED);        
                }
                else{
                    BitSet(data,RF_L_FAULT_LED);
                }
                if ((Clock2%ModulationLED)==0){
                    BitRST(data,DISABLE_LED);
                    //BitRST(data,RF_L_FAULT_LED);  
                    //BitRST(data,RF_B_FAULT_LED);             
                }
           break;
           default:
                BitSet(data,DISABLE_LED);         
                if (BitTest(LearnStatus,BackboneLearnMode) == 1){
                    BitSet(data,RF_B_FAULT_LED);        
                }
                else{
                    BitSet(data,RF_L_FAULT_LED);
                }
                if ((Clock2%ModulationLED)==0){
                    BitRST(data,DISABLE_LED);
                    BitRST(data,RF_L_FAULT_LED);  
                    BitRST(data,RF_B_FAULT_LED);             
                }
           break;
           }   
    } 
    if (BitTest(input,L_ALARM_bit)==1){
        BitSet(data,L_ALARM_LED);              
    }
    if (BitTest(input,R_ALARM_bit)==1){
        BitSet(data,R_ALARM_LED);              
    }
    if (BitTest(input,FAULT_bit)==1){
        BitSet(data,FAULT_LED);     
    }
    if (BitTest(input,P_FAULT_bit)==1){
        BitSet(data,P_FAULT_LED);
    }               
    if (BitTest(input,POWER_bit)==1){
        if (blink_LED == 1){
            BitSet(data,POWER_LED);
        }
        BitSet(data,P_FAULT_LED);
    }   
    else{
        BitSet(data,POWER_LED);
    }     
    if (BitTest(input,DISABLE_bit)==1){
        BitSet(data,DISABLE_LED);
    }
    if (BitTest(input,LEVEL2_bit)==1){
        BitSet(data,LEVEL2_LED);              
    }
    /*else if (RFmoduleBusy==1){
        if (blink_LED == 1){
            BitSet(data,LEVEL2_LED);
        }
    } */
    if (BitTest(input,TEST_bit)==1){
        BitSet(data,TEST_LED);              
    }
    else if (BitTest(testRegister,RFattenuation) == 1){
        if (blink_LED == 1){
            BitSet(data,TEST_LED);
        }
    } 
    
    if (BitTest(input,HUSH_bit)==1){
        BitSet(data,HUSH_LED);              
    }
    if (BitTest(input,SYS_FAULT_bit)==1){
        BitSet(data,SYS_FAULT_LED);
        BitSet(data,FAULT_LED);  
    }
    if (BitTest(input,TESTMODE_bit)==1){
        BitSet(data,BATTERY_LED);
    }  
    if (BitTest(input,L_BATTERY_bit)==1){
        BitSet(data,L_BATTERY_LED);  
        BitSet(data,FAULT_LED); 
    }
    if (BitTest(input,TESTMODE_bit)==0){
        if ((BitTest(input,L_ALARM_bit)==1)||(BitTest(input,R_ALARM_bit)==1)){
            RELE_ALARM = 1;
        }
        else{ 
            RELE_ALARM = 0;
        }
    }  
    if ((BitTest(input,FAULT_bit)==1)||(BitTest(input,L_BATTERY_bit)==1)||(BitTest(input,RF_L_FAULT_bit)==1)){
        if (BitTest(input,TESTMODE_bit)==0){
            RELE_FAULT = 0;                   
        }
    }
    else if ((BitTest(input,P_FAULT_bit)==1)||(BitTest(input,POWER_bit)==1)||(BitTest(input,SYS_FAULT_bit)==1)){ 
        RELE_FAULT = 0;      
    }
    else{
        RELE_FAULT = 1;
    }      
    if (BitTest(input,RF_L_FAULT_bit)==1){
        BitSet(data,RF_L_FAULT_LED);              
    }
    if (BitTest(input,RF_B_FAULT_bit)==1){
        BitSet(data,RF_B_FAULT_LED);              
    }                  
    if (BitTest(input,RS485_FAULT_bit)==1){
        BitSet(data,RS485_FAULT_LED);              
    }                                 
    /*if (LearnMode==1){
        BitSet(data,LEVEL2_bit);              
    }
    else{
        BitRST(data,LEVEL2_bit);
    } */
    /*if (BitTest(input,DISABLE_bit)== 0){
        if (pom==1){
            BitSet(data,DISABLE_bit);              
        }
        else{
            BitRST(data,DISABLE_bit);
        }                                   
    }*/ 
    
    if ((BitTest(input,L_ALARM_bit)==1)||(BitTest(input,R_ALARM_bit)==1)){
        if (silence==0){
            SET_INT_SIREN_SW;
        }
    } 
    else if (BitTest(input,FAULT_bit)==1){
        modulatorSIREN = DFAULT_ModulationKonst;
        if (silence==0){ 
            if ((Clock2%modulatorSIREN)==0){
                SET_INT_SIREN_SW;              
            }   
            else{
                CLR_INT_SIREN_SW;
            }
        } 
    }
    else if (BitTest(input,L_BATTERY_bit)==1){
        modulatorSIREN = BFAULT_ModulationKonst;
        if (silence==0){ 
            if ((Clock2%modulatorSIREN)==0){
                SET_INT_SIREN_SW;              
            }   
            else{
                CLR_INT_SIREN_SW;
            }
        } 
    }
    else if ((BitTest(input,P_FAULT_bit)==1)||(BitTest(input,POWER_bit)==1)){
        modulatorSIREN = PFAULT_ModulationKonst;
        if (silence==0){ 
            if ((Clock2%modulatorSIREN)==0){
                SET_INT_SIREN_SW;              
            }   
            else{
                CLR_INT_SIREN_SW;
            }
        } 
    }
    else if (BitTest(input,SYS_FAULT_bit)==1){
        modulatorSIREN = SFAULT_ModulationKonst;
        if (silence==0){ 
            if ((Clock2%modulatorSIREN)==0){
                SET_INT_SIREN_SW;              
            }   
            else{
                CLR_INT_SIREN_SW;
            }
        } 
    }
    else if (BitTest(input,RF_L_FAULT_bit)==1){
        modulatorSIREN = COMFAULT_ModulationKonst;
        if (silence==0){ 
            if ((Clock2%modulatorSIREN)==0){
                SET_INT_SIREN_SW;              
            }   
            else{
                CLR_INT_SIREN_SW;
            }
        }              
    }
    else if (BitTest(input,RF_B_FAULT_bit)==1){
        modulatorSIREN = BACKFAULT_ModulationKonst;
        if (silence==0){ 
            if ((Clock2%modulatorSIREN)==0){
                SET_INT_SIREN_SW;              
            }   
            else{
                CLR_INT_SIREN_SW;
            }
        }              
    }                  
    else if (BitTest(input,RS485_FAULT_bit)==1){
        modulatorSIREN = RS485FAULT_ModulationKonst;
        if (silence==0){ 
            if ((Clock2%modulatorSIREN)==0){
                SET_INT_SIREN_SW;              
            }   
            else{
                CLR_INT_SIREN_SW;
            }
        }              
    } 
    else if (BitTest(input,HUSH_bit)==1){
        modulatorSIREN = HUSH_ModulationKonst;
        if (silence==0){ 
            if ((Clock2%modulatorSIREN)==0){
                SET_INT_SIREN_SW;              
            }   
            else{
                CLR_INT_SIREN_SW;
            }
        } 
    }
    else if ((BitTest(input,TEST_bit)==1)||(BitTest(testRegister,RFattenuation) == 1)){
        modulatorSIREN = TEST_ModulationKonst;
        if (silence==0){ 
            if ((Clock2%modulatorSIREN)==0){
                SET_INT_SIREN_SW;              
            }   
            else{
                CLR_INT_SIREN_SW;
            }
        } 
    }
    else{      
        silence = 0; 
        CLR_INT_SIREN_SW;
    }    
    if (RFmoduleRST == 1){
        data = 0xFFFF;               
    }
    //data = LoopEventRAM[0];
    return(data);
}

unsigned int Indication2(unsigned int input){
unsigned int data;
    data = 0;  
    /*if (BitTest(input,POWER_bit)==1){
        BitSet(data,POWER_LED);              
    }
    if (BitTest(input,L_ALARM_bit)==1){
        BitSet(data,L_ALARM_LED);              
    }
    if (BitTest(input,R_ALARM_bit)==1){
        BitSet(data,R_ALARM_LED);              
    }
    if (BitTest(input,BATTERY_bit)==1){
        BitSet(data,BATTERY_LED);              
    }
    if (BitTest(input,L_BATTERY_bit)==1){
        BitSet(data,L_BATTERY_LED);              
    }
    if (BitTest(input,LEVEL2_bit)==1){
        BitSet(data,LEVEL2_LED);              
    }
    if (BitTest(input,RF_L_FAULT_bit)==1){
        BitSet(data,RF_L_FAULT_LED);              
    }
    if (BitTest(input,RF_B_FAULT_bit)==1){
        BitSet(data,RF_B_FAULT_LED);              
    }
    if (BitTest(input,RS485_FAULT_bit)==1){
        BitSet(data,RS485_FAULT_LED);              
    }
    if (BitTest(input,HUSH_bit)==1){
        BitSet(data,HUSH_LED);              
    }
    if (BitTest(input,TEST_bit)==1){
        BitSet(data,TEST_LED);              
    }
    if (BitTest(input,DISABLE_bit)==1){
        BitSet(data,DISABLE_LED);              
    }
    if (BitTest(input,SYS_FAULT_bit)==1){
        BitSet(data,SYS_FAULT_LED);              
    }
    if (BitTest(input,P_FAULT_bit)==1){
        BitSet(data,P_FAULT_LED);              
    }
    if (BitTest(input,FAULT_bit)==1){
        BitSet(data,FAULT_LED);              
    }*/   
    modulatorSIREN = DFAULT_ModulationKonst;
    if ((Clock2%modulatorSIREN)==0){
        SET_INT_SIREN_SW;              
    }   
    else{
        CLR_INT_SIREN_SW;
    }
    
    if (input==0){
        BitSet(data,POWER_LED);              
    }
    if (input==1){
        BitSet(data,L_ALARM_LED);              
    }
    if (input==2){
        BitSet(data,R_ALARM_LED);              
    }
    if (input==3){
        BitSet(data,BATTERY_LED);              
    }
    if (input==4){
        BitSet(data,L_BATTERY_LED);              
    }
    if (input==5){
        BitSet(data,LEVEL2_LED);              
    }
    if (input==6){
        BitSet(data,RF_L_FAULT_LED);              
    }
    if (input==7){
        BitSet(data,RF_B_FAULT_LED);              
    }
    if (input==8){
        BitSet(data,RS485_FAULT_LED);              
    }
    if (input==9){
        BitSet(data,HUSH_LED);              
    }
    if (input==10){
        BitSet(data,TEST_LED);              
    }
    if (input==11){
        BitSet(data,DISABLE_LED);              
    }
    if (input==12){
        BitSet(data,SYS_FAULT_LED);              
    }
    if (input==13){
        BitSet(data,P_FAULT_LED);              
    }
    if (input==14){
        BitSet(data,FAULT_LED);              
    }
    return(data);
}

void SPI_Transfer2LED(unsigned int data){
unsigned char dataL,dataH;
    dataL = data;
    dataH = data>>8;
    RST_LED = 1; 
    delay_us(10);
    RST_LED = 0; 
    delay_us(1);
    SS_LED = 1;
    delay_us(10);
    SS_LED = 0;
    // SPI initialization
    // SPI Type: Master
    // SPI Clock Rate: 250,000 kHz
    // SPI Clock Phase: Cycle Start
    // SPI Clock Polarity: Low
    // SPI Data Order: MSB First
    SPCR=0x51;
    SPSR=0x00;
    SPDR=dataH;  
    //delay_ms(100);
    while(!(SPSR&(1<<SPIF)));
    SPCR=0x51;
    SPSR=0x00;
    SPDR=dataL;               
    while(!(SPSR&(1<<SPIF)));
    SPCR=0x00;
    SPSR=0x00;  
    delay_us(5);
    SS_LED = 1;
    delay_us(10);
    SS_LED = 0;  
    MOSI=0;
}

void SPI_Byte2RF(unsigned char data){
    SS_RF = 0;
    delay_us(40);
    // SPI initialization
    // SPI Type: Master
    // SPI Clock Rate: 250,000 kHz
    // SPI Clock Phase: Cycle Start
    // SPI Clock Polarity: Low
    // SPI Data Order: MSB First
    SPCR=0x51;
    SPSR=0x00;
    SPDR=data;                   
    //delay_ms(100);
    while(!(SPSR&(1<<SPIF)));
    delay_us(40);
    SS_RF = 1;       
    delay_us(250);
    delay_us(250); 
    delay_us(20);
}

unsigned char SPI_Transfer2RF(unsigned char SPIDLEN){
unsigned char dataAUX,CRCM,CRCS,i;
    SPI_Byte2RF(SPI_CHECK);         //First sends SPI_CHECK
    SPI_Byte2RF(SPI_CHECK);         //First sends SPI_CHECK
    dataAUX = SPDR;
    if  (dataAUX == 0x80){
        SPI_Byte2RF(SPI_CMD);
        SPI_Byte2RF(PTYPE+SPIDLEN);
        CRCM = SPI_CMD^(PTYPE+SPIDLEN);
        CRCS = (PTYPE+SPIDLEN);               
        for(i = 0; i<SPIDLEN;++i){
            SPI_Byte2RF(SPI_message2RF[i]);
            SPI_message2CPU[i] = SPDR; 
            CRCM = CRCM^SPI_message2RF[i]; 
            CRCS = CRCS^SPI_message2CPU[i];      
        }
        CRCM = CRCM^0x5F; 
        CRCS = CRCS^0x5F; 
        SPI_Byte2RF(CRCM); 
        dataAUX = SPDR;          
        SPI_Byte2RF(SPI_CHECK);         
        if  ((SPDR == 0x3F)&&(dataAUX == CRCS)){
            dataAUX = SPDR;
        }                  
        else{
            dataAUX = 0xFF;
        }
    }
    else{ 
        dataAUX = 0xFF;
    } 
    SPCR=0x00;
    SPSR=0x00;
    MOSI=0;
    return(dataAUX);               
}

//unsigned char SPI_Transfer2RF(unsigned char data){
//unsigned char dataAUX,CRCM;
//    SPI_Byte2RF(SPI_CHECK);         //First sends SPI_CHECK
//    SPI_Byte2RF(SPI_CHECK);         //First sends SPI_CHECK
//    dataAUX = SPDR;
//    //if  (dataAUX == 0x3F){
//    //    Level2 = 1;
//    //}
//    //else{
//    //    Level2 = 0;
//    //}
//    //if  (dataAUX == 0x3E){
//    //    BitSet(LoopEventRAM[Panel_LoopID],Test_bit);
//    //}
//    //else{
//    //    BitRST(LoopEventRAM[Panel_LoopID],Test_bit);
//    //}
//    if  (dataAUX == 0x80){
//        //BitSet(LoopEventRAM[Panel_LoopID],Hush_bit);
//        SPI_Byte2RF(SPI_CMD);
//        SPI_Byte2RF(PTYPE);               
//        SPI_Byte2RF(data);
//        dataAUX = SPDR;
//        CRCM = SPI_CMD^PTYPE^data^0x5F; 
//        SPI_Byte2RF(CRCM);             
//        SPI_Byte2RF(SPI_CHECK);         
//        if  (SPDR != 0x3F){
//            dataAUX = 0xFF;
//        }
//    }
//    else{
//        dataAUX = 0xFF;
//        //BitRST(LoopEventRAM[Panel_LoopID],Hush_bit);
//    }
//    return(dataAUX);               
//}

void button_pressed(unsigned char *level,unsigned char *mode){//Time 220ms
    
    unsigned char butt1,butt2,i,conf1,conf2;
    unsigned char filtr1[10],filtr2[10];
    conf1=0;
    conf2=0;           
    butt1=0x07;
    butt2=0x07;
    i = 0;
    if (pushbutton_pressed == 1){
        delay_msWDR(100);
        for(i=0;i<10;i++){
            filtr1[i]=0x07&TL;
            delay_msWDR(5);
        }
        for(i=0;i<10;i++){
            if (filtr1[i]<0x07){
                if (butt1==filtr1[i]){
                    conf1++;  
                }
                else{
                    butt1=filtr1[i];
                    conf1=0;
                }  
            }
        }
        delay_msWDR(10);
        for(i=0;i<10;i++){
            filtr2[i]=0x07&TL;
            delay_msWDR(1);
        }
        for(i=0;i<10;i++){
            if (filtr2[i]<0x07){
                if (butt2==filtr2[i]){
                    conf2++;  
                }
                else{
                    butt2=filtr2[i];
                    conf2=0;
                }    
            }
        }                             
        if ((butt1<0x07)||(butt2<0x07)){
            //test of pressed button for Flip-Flop D
            if((butt1==butt2)&&(conf2>8)&&(conf1>8)){   
                NEG_INT_SIREN_SW;
                delay_msWDR(50);
                NEG_INT_SIREN_SW;
                button = butt2;
                //time_last_switch = 1;
            }   
            else{
                button = 0x07;
            }                                             
        }
        else{
            button = 0x07;
        }       
    }
    else{
        button = 0x07;
    }
    
    if (button<0x07){
         Event = 1;
         Clock3 = 0;
         if (Level3 == 0){
            if (BitTest(Panel_Status,LEVEL2_bit)==0){
                *level=button_function_level1(button,*level);    
            }                                
            else{
                *level=button_function_level2(button,*level);
                
            
 
            }
         }
         
         else if (Level3 < 2){
            *level=button_function_level3(button,*level);
         }
             
         else{
            *level=button_function_level4(button,*level);
         }
         /*if (Panic_programing_on==0){
            *level=button_function_level1(button,*level);
         }
         else{    
            Panic_programing_index=button_function_level2(button,Panic_programing_index);
         } */
         //pressed_button = 1;
    }
    else{
         *mode = 0x00; 
         //(Jp) set of the full delay in case the buttons were not pressed sufficient time
        if (( L3Cnt >0)&&(L3Cnt < L3_delay)){
        L3Cnt = L3_delay;
         } 
    } 
    //(Jp) set for continue of the  button evalaution during delay  
    if (( L3Cnt >0)&&(L3Cnt < L3_delay)) {
      pushbutton_pressed = 1;
     }
    else
       {
       pushbutton_pressed = 0;
       }     
}

unsigned char button_function_level1(unsigned char butt,unsigned char level){
unsigned char y,z,i,Low,High,a; 
    if ((BitTest(LearnStatus,RemoteLearnMode) == 1)||(BitTest(LearnStatus,LearnMode) == 1)){
        if (butt != 0x01){
           butt = 0;
        }
    }   
    switch (butt) {
        case 0x03:      //TL1 -- SIREN
            if (ProductionMode==0){
                silence = 1;  
                CLR_INT_SIREN_SW; 
                SPI_message2RF[SPI_ID] = NodeID_origin;
                SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
                SPI_message2RF[SPI_ZONE] = ZoneID_origin;
                //SPI_message2RF[SPI_COMMAND] = 'T';  
                if ((BitTest(Panel_Status,L_ALARM_bit)==1)||(BitTest(Panel_Status,R_ALARM_bit)==1)){
                    SPI_message2RF[SPI_COMMAND] = 'Q'; 
                    RS485Message_Construct(NodeID_origin,SystemID_origin,ZoneID_origin,'R');
                    //RS485Message_Construct('R');    
                }
                else if ((BitTest(Panel_Status,FAULT_bit)==1)||(BitTest(Panel_Status,L_BATTERY_bit)==1)||(BitTest(Panel_Status,RF_L_FAULT_bit)==1)){
                    SPI_message2RF[SPI_COMMAND] = 'Q';
                } 
            }
            else{
                SET_INT_SIREN_SW;
                while(TL != 0x07){
                    #asm("wdr");
                }
            }
            break;
        case 0x06:     //TL2 -- TEST       //1800ms
            if (ProductionMode==0){   
                #asm("wdr");
                NEG_INT_SIREN_SW;
                SPI_Transfer2LED(0xFFFF);
                delay_msWDR(200);
                delay_msWDR(200);
                SPI_Transfer2LED(0x0000);            
                NEG_INT_SIREN_SW;  
                delay_msWDR(200);         //600ms
                if (RFunprogramed == 0){
                    RFEventHistoryOUT();    
                }
                /*SPI_message2RF[SPI_ID] = 2;
                SPI_message2RF[SPI_SUBLOOP] = 3;
                SPI_message2RF[SPI_ZONE] = 4;
                SPI_message2RF[SPI_COMMAND] = 'F';*/
            }
            else{
                SPI_Transfer2LED(0xFFFF);
                while(TL != 0x07){
                    #asm("wdr");
                }
                SPI_Transfer2LED(0x0000);
            }
            break;
        case 0x01:     //TL1+TL2 -- SIREN + RESET
            if (BitTest(LearnStatus,RemoteLearnMode) == 1){
                LearnModeTimer = LearnModeTimerMAX-2;
            }
            else{
                if ((LearnModeTimer < (LearnModeTimerMAX-1))&&(BitTest(LearnStatus,LearnMode) == 1)){
                    if (BitTest(LearnStatus,BackboneLearnMode) == 0){
                        BitSet(LearnStatus,BackboneLearnMode);
                    }
                    else{
                        BitRST(LearnStatus,BackboneLearnMode);
                    } 
                }
                LearnModeTimer = 0;                
                if (BitTest(LearnStatus,LearnMode) == 1){
                    if (BitTest(LearnStatus,LearnModeExit) == 1){
                        BitRST(LearnStatus,LearnMode);
                        LearnModeTimer = LearnModeTimerMAX-2;       
                    } 
                }             
                else{
                    BitSet(LearnStatus,LearnMode);  
                }                      
            }
            break;
        case 0x04:     //TL3+TL2 -- TEST + RESET 
            i = 0; 
            while ((TL == 0x04)&&(i<30)){
                delay_msWDR(200);
                #asm("wdr");
                ++i;
            }
            if (i > 29){ 
                SPI_message2RF[SPI_ID] = NodeID_origin;
                SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
                SPI_message2RF[SPI_ZONE] = ZoneID_origin;
                SPI_message2RF[SPI_COMMAND] = 'D'; 
                RFmoduleRST = 1;
            }
            break;
        case 0x02:     //TL1+TL3 -- SIREN + TEST  //150ms 
            L3Cnt = L3_delay; //(Jp) set of the counter ( delay) for entry into the Level3 
            
            BitSet(Panel_Status,LEVEL2_bit);
            delay_msWDR(50);
            SET_INT_SIREN_SW;
            delay_msWDR(100);  
            CLR_INT_SIREN_SW;
            break;
        case 0x05:     //TL2 -- RESET
            /*if (ProductionMode==0){
                CLR_INT_SIREN_SW;
                ResetMessage();
                RFmoduleRST = 1;
                SPI_message2RF[SPI_ID] = NodeID_origin;
                SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
                SPI_message2RF[SPI_ZONE] = ZoneID_origin;
                SPI_message2RF[SPI_COMMAND] = 'R'; 
                RS485Message_Construct(NodeID_origin,SystemID_origin,ZoneID_origin,'R');
            }
            else{
                #asm("wdr");
                WDTCR=(1<<WDTOE) | (1<<WDE) | (1<<WDP2) | (1<<WDP1) | (1<<WDP0);
                WDTCR=(0<<WDTOE) | (0<<WDE) | (1<<WDP2) | (1<<WDP1) | (1<<WDP0);
                SPI_Transfer2LED(0x0000); 
                #asm("cli")   
                MCUCR = 0x30;
                MCUCR |= 0x40; 
                #asm("sleep")       //Sleep for 62,5ms 
                #asm("nop")
                #asm("nop")
            }*/  
            if (ProductionMode!=0){  
                #asm("wdr");
                WDTCR=(1<<WDTOE) | (1<<WDE) | (1<<WDP2) | (1<<WDP1) | (1<<WDP0);
                WDTCR=(0<<WDTOE) | (0<<WDE) | (1<<WDP2) | (1<<WDP1) | (1<<WDP0);
                Check_Memory_Integrity();
                SPI_Transfer2LED(0x0000); 
                #asm("cli")   
                MCUCR = 0x30;
                MCUCR |= 0x40; 
                #asm("sleep")       //Sleep for 62,5ms 
                #asm("nop")
                #asm("nop")
            }
            break;
    }                         
    return(level);
}

unsigned char button_function_level2(unsigned char butt,unsigned char level){
unsigned char i;
    BitRST(Panel_Status,LEVEL2_bit); 
                
    switch (butt) {
        case 0x03:      //TL1 -- SIREN 
            delay_msWDR(200);
            if (0x07&TL == 0x02){    //TL1+TL3 -- SIREN + TEST
                SPI_message2RF[SPI_ID] = NodeID_origin;
                SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
                SPI_message2RF[SPI_ZONE] = ZoneID_origin;
                SPI_message2RF[SPI_COMMAND] = 'T';        
                BitSet(LoopEventRAM[SPI_message2RF[SPI_ID]],Test_bit);     
                RS485Message_Construct(NodeID_origin,SystemID_origin,ZoneID_origin,'T');
            }
            else{
                silence = 1; 
                CLR_INT_SIREN_SW; 
                SPI_message2RF[SPI_ID] = NodeID_origin;
                SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
                SPI_message2RF[SPI_ZONE] = ZoneID_origin; 
                if ((BitTest(Panel_Status,L_ALARM_bit)==1)||(BitTest(Panel_Status,R_ALARM_bit)==1)){
                    SPI_message2RF[SPI_COMMAND] = 'Q'; 
                    RS485Message_Construct(NodeID_origin,SystemID_origin,ZoneID_origin,'R');    
                }
                else if ((BitTest(Panel_Status,FAULT_bit)==1)||(BitTest(Panel_Status,L_BATTERY_bit)==1)||(BitTest(Panel_Status,RF_L_FAULT_bit)==1)){
                    SPI_message2RF[SPI_COMMAND] = 'Q';
                }
            }
            break;
        case 0x06:     //TL3 -- TEST
            delay_msWDR(200);
            if (0x07&TL == 0x02){    //TL1+TL3 -- TEST + SIREN               
                SPI_message2RF[SPI_ID] = NodeID_origin;
                SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
                SPI_message2RF[SPI_ZONE] = ZoneID_origin;
                SPI_message2RF[SPI_COMMAND] = 'H';
                BitSet(LoopEventRAM[SPI_message2RF[SPI_ID]],Hush_bit);
                RS485Message_Construct(NodeID_origin,SystemID_origin,ZoneID_origin,'H');
            }
            else{
                NEG_INT_SIREN_SW;
                SPI_Transfer2LED(0xFFFF);
                delay_msWDR(200);
                delay_msWDR(200);
                SPI_Transfer2LED(0x0000);            
                NEG_INT_SIREN_SW;  
                delay_msWDR(200); 
                RFEventHistoryOUT();
                if (BitTest(testRegister,TestMode)==1){    
                    BitRST(testRegister,TestMode);        
                }                
                else{
                    BitSet(testRegister,TestMode); 
                }
            }
            /*SPI_message2CPU[SPI_ID]=SPI_message2RF[SPI_ID];
            SPI_message2CPU[SPI_ZONE]=SPI_message2RF[SPI_ZONE];
            SPI_message2CPU[SPI_SUBLOOP]=SPI_message2RF[SPI_SUBLOOP];
            RS485Message_Construct('T');*/
            break;
        case 0x01:     //TL1+TL2 -- SIREN + RESET
            break;
        case 0x02:     //TL1+TL3 -- SIREN + TEST 
       
     // (Jp) if 5s delay is not expired no entry into Level3,  short sound only
        
        if (L3Cnt > 1){
         
         L3Cnt = L3Cnt - 1;  // decrements counter of the running delay
                             //and then  do the same as for enter into Level2 
         BitSet(Panel_Status,LEVEL2_bit); // to stay in Level2
         delay_msWDR(100);  
            
           
         pushbutton_pressed = 1;  // simuluje uvoln�n� tla��tka pro dal�� testovac� cyklus
             
            break;                 
         }
        else                // else do the origianal procedure for Level3 enter
           { 
            L3Cnt = 0;     // (Jp) reset of the delay counter / new setwill be possible only at the momemt 
                             // of change from Level1 to Level2 
            
            RS485_maxADR = 0;
            RS485_IQnetADR = 0; 
            RS485_IQnet = 0;
            EEPROM_write(EEPROM_store+EEnmbRS485,RS485_maxADR);
            Level3 = 1;
            silence = 0;  
            delay_msWDR(50);           
            NEG_INT_SIREN_SW;
            delay_msWDR(100);
            NEG_INT_SIREN_SW;
            delay_msWDR(50);
            NEG_INT_SIREN_SW;
            delay_msWDR(50);
            NEG_INT_SIREN_SW;
            delay_msWDR(50);
            NEG_INT_SIREN_SW;
            delay_msWDR(50);
            CLR_INT_SIREN_SW;
            }

            break;
        case 0x05:     //TL2 -- RESET                   
            if (BitTest(Panel_Status,DISABLE_bit) == 0){
                i = 0; 
                while ((TL == 0x05)&&(i<10)){
                    delay_msWDR(200);
                    #asm("wdr");
                    ++i;
                }
                if (i < 10){
                    CLR_INT_SIREN_SW;
                    ResetMessage();
                    RFmoduleRST = 1;
                    SPI_message2RF[SPI_ID] = NodeID_origin;
                    SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
                    SPI_message2RF[SPI_ZONE] = ZoneID_origin;
                    SPI_message2RF[SPI_COMMAND] = 'R'; 
                    /*for(i = 0; i<32; ++i){
                        LoopEventRAM[i] = 0;
                    }
                    for(i = 0; i<32; ++i){
                        BackboneEventRAM[i] = 0;
                    }  */
                    /*SPI_message2CPU[SPI_ID]=SPI_message2RF[SPI_ID];
                    SPI_message2CPU[SPI_ZONE]=SPI_message2RF[SPI_ZONE];
                    SPI_message2CPU[SPI_SUBLOOP]=SPI_message2RF[SPI_SUBLOOP];
                    RS485Message_Construct('R');  */
                    RS485Message_Construct(NodeID_origin,SystemID_origin,ZoneID_origin,'R');
                }
                else{
                    BitSet(Panel_Status,DISABLE_bit);
                    SPI_Transfer2LED(0x0001<<POWER_LED);
                    delay_msWDR(50);
                    SPI_Transfer2LED(0x0001<<L_ALARM_LED);
                    delay_msWDR(50);
                    SPI_Transfer2LED(0x0001<<R_ALARM_LED);
                    delay_msWDR(50);
                    SPI_Transfer2LED(0x01<<BATTERY_LED);
                    delay_msWDR(50);
                    SPI_Transfer2LED(0x01<<L_BATTERY_LED);
                    delay_msWDR(50);
                    SPI_Transfer2LED(0x01<<LEVEL2_LED);
                    delay_msWDR(50);
                    SPI_Transfer2LED(0x01<<RF_L_FAULT_LED);
                    delay_msWDR(50);
                    SPI_Transfer2LED(0x01<<RF_B_FAULT_LED);
                    delay_msWDR(50);
                    SPI_Transfer2LED(0x01<<RS485_FAULT_LED);
                    delay_msWDR(50);
                    SPI_Transfer2LED(0x01<<HUSH_LED);
                    delay_msWDR(50);
                    SPI_Transfer2LED(0x01<<TEST_LED);
                    delay_msWDR(50);
                    SPI_Transfer2LED(0x01<<DISABLE_LED);
                    delay_msWDR(50);
                    SPI_Transfer2LED(0x01<<SYS_FAULT_LED);
                    delay_msWDR(50);
                    SPI_Transfer2LED(0x01<<P_FAULT_LED); 
                    delay_msWDR(50);
                    SPI_Transfer2LED(0x01<<FAULT_LED); 
                    delay_msWDR(50);
                    SPI_Transfer2LED(0xFFFF);
                    RFEventsDelete();
                }      
            }   
            else{
                #asm("wdr");
                BitRST(Panel_Status,DISABLE_bit); 
                RFmoduleRestart();
            }
            break;
    }  
   //(Jp) REset L3Cnt if No Level2 more 
    if (BitTest(Panel_Status,LEVEL2_bit) == 0)
       {
         L3Cnt = 0;
       }                    
    return(level);
}

unsigned char button_function_level3(unsigned char butt,unsigned char level){
    switch (butt) {
        case 0x03:      //TL1 -- SIREN
            delay_msWDR(200);
            delay_msWDR(200);
            if (0x07&TL == 0x02){    //TL1+TL3 -- SIREN + TEST
                SPI_message2RF[SPI_ID] = NodeID_origin;
                SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
                SPI_message2RF[SPI_ZONE] = ZoneID_origin;
                SPI_message2RF[SPI_COMMAND] = '1';
                Level3 = 0;   
                BitSet(testRegister,RFattenuation);
                BitSet(testRegister,TestInstallation);
            }
            else if (RS485_maxADR < 14){ 
                ++RS485_maxADR;
            }
            break;
        case 0x06:     //TL3 -- TEST
            if(RS485_maxADR > 0){
                --RS485_maxADR;       
            }             
            break;
        case 0x04:     //TL3+TL2 -- TEST + RESET
            break;
        case 0x02:     //TL1+TL3 -- SIREN + TEST
            Level3 = 2; 
            EEPROM_write(EEPROM_store+EEnmbRS485,RS485_maxADR);
            delay_msWDR(50);           
            NEG_INT_SIREN_SW;
            delay_msWDR(100);
            NEG_INT_SIREN_SW;
            delay_msWDR(50);
            NEG_INT_SIREN_SW;
            delay_msWDR(50);
            NEG_INT_SIREN_SW;
            delay_msWDR(50);
            NEG_INT_SIREN_SW;
            delay_msWDR(50);
            CLR_INT_SIREN_SW;
            break;
        case 0x05:     //TL2 -- RESET                   
            SPI_message2RF[SPI_COMMAND] = 'P';
            break;
    }                         
    return(level);
}

unsigned char button_function_level4(unsigned char butt,unsigned char level){
    switch (butt) {
        case 0x03:      //TL1 -- SIREN
            if ((RS485_IQnetADR < 14)&&(RS485_maxADR > 0)){ 
                ++RS485_IQnetADR;
            }
            break;
        case 0x06:     //TL3 -- TEST
            if(RS485_IQnetADR > 0){
                --RS485_IQnetADR;       
            }
            else{
                RS485_IQnet = 0; 
            }             
            break;
        case 0x01:     //TL1+TL2 -- SIREN + RESET
            //Level3 = 0; 
            //EEPROM_write(EEPROM_store+EEnmbRS485,RS485_maxADR);
            break;
        case 0x02:     //TL1+TL3 -- SIREN + TEST
            RS485_IQnet = 1;
            if (RS485_IQnetADR > 0){ 
                RS485_IQnet = 1;
            }
            Level3 = 0;  
            EEPROM_write(EEPROM_store+EEadrRS485,RS485_IQnetADR);
            delay_msWDR(50);           
            NEG_INT_SIREN_SW;
            delay_msWDR(100);
            NEG_INT_SIREN_SW;
            delay_msWDR(50);
            NEG_INT_SIREN_SW;
            delay_msWDR(50);
            NEG_INT_SIREN_SW;
            delay_msWDR(50);
            NEG_INT_SIREN_SW;
            delay_msWDR(50);
            CLR_INT_SIREN_SW;
            break;
        case 0x05:     //TL2 -- RESET                   
            SPI_message2RF[SPI_COMMAND] = 'P';
            break;
    }                         
    return(level);
}

void Power_indication_filter(){
//unsigned char Line1,Line2; 
    Line1 = 0;
    Line2 = 0;    
    if (POWER_MAINS == 1){
        if (PM_Filter1<250)
        {
            PM_Filter1++;       
        } 
    }                                                         
    
    if (POWER_FAULT == 1){
        if (PF_Filter2<250)
        {
            PF_Filter2++;
        } 
    }                                                         
          
    if (PM_Filter1_timer >= PFilter1_timer_END){
        if (PM_Filter1 > 1){
            BitSet(Panel_Status,POWER_bit);
        }
        else{         
            BitRST(Panel_Status,POWER_bit);
        }                  
        PM_Filter1=0;
        PM_Filter1_timer = 0;
    } 
      
    init_adc();
    Line1 = read_adc(4);
    off_adc();
    init_adc();
    Line2 = read_adc(5);
    off_adc();
    
    if (BitTest(Panel_Status,POWER_bit) == 1){
        Event = 1;      
    }                         
    if (PF_Filter2_timer >= PFilter2_timer_END){
        if ((PF_Filter2 > 1)||(Line1 < 0x3C)||(Line2 < 0x3C)){      //voltege on the line 1 and 2 0x3C = 0.8V
            BitSet(LoopEventRAM[NodeID_origin],Power_Fault_bit);  
        }
        else{
            BitRST(LoopEventRAM[NodeID_origin],Power_Fault_bit);  
        }   
        Event = 1;              
        PF_Filter2=0;
        PF_Filter2_timer = 0;
    }              
}

#define RS485SynchroNMB_old   80
#define RS485SynchroNMB       80
#define RS485Message_Length   6
#define RS485Message_LengthIQ (11-RS485Message_Length)
#define START_SEQUENCE        0xFF
#define STOP_SEQUENCE         0xFE

#define START_B         0
#define STOP_B          RS485Message_Length-1

#define LOOP_ID_IQ      3
#define DET_ID_IQ       4
#define ZONE_ID_IQ      5
#define SUBLOOP_ID_IQ   6
#define ADDR_B_IQ       7
//#define LENGTH_B_IQ     6
#define COMM_B_IQ       8
#define CRC_B_IQ        9
#define STOP_B_IQ       10

#define COMM_B          3
#define LENGTH_B        2
#define ADDR_B          1
#define CRC_B           4


#define RS485DelayMin2TX      2
#define IQ_Marker       'I'
// USART Transmitter interrupt service routine
interrupt [USART_TXC] void usart_tx_isr(void)
{ 
unsigned char data,i;
    #asm("cli")         
    while (!(UCSRA & (1<<UDRE)));
    if (RS485_IQnet){
        if (RS485_MessageIndex < RS485SynchroNMB){
            ++RS485_MessageIndex;
            data = 0;
        }
        else if (RS485_MessageIndex <= (RS485SynchroNMB+messageLength)){
            i=RS485_MessageIndex-RS485SynchroNMB;
            data = messageRS485TX[i];
            ++RS485_MessageIndex;
        }
        if (RS485_MessageIndex > (RS485SynchroNMB+messageLength)){ 
            RS485_TXEN = 0;
            RS485_MessageIndex=0;       
            ++RS485_MessageRepeat;
            RS485_Timing = 0;
            //delay_msWDR(100);
            if (RS485_MessageRepeat==6){
                RS485Send = 0;
                RS485_MessageRepeat = 0;
                //UDR = 0;     
            }      
        }          
        else{         
            UDR = data;
        }
    }
    else{
        if (RS485_MessageIndex < RS485SynchroNMB_old){
            ++RS485_MessageIndex;
            data = 0;
        }
        else if (RS485_MessageIndex <= (RS485SynchroNMB_old+messageLength)){
            i=RS485_MessageIndex-RS485SynchroNMB_old;
            data = messageRS485TX[i];
            ++RS485_MessageIndex;
        }
        if (RS485_MessageIndex > (RS485SynchroNMB_old+messageLength)){ 
            RS485_TXEN = 0;
            RS485_MessageIndex=0;       
            ++RS485_MessageRepeat;
            RS485_Timing = 0;
            //delay_msWDR(100);
            if (RS485_MessageRepeat==6){
                RS485Send = 0;
                RS485_MessageRepeat = 0;
                //UDR = 0;     
            }      
        }          
        else{         
            UDR = data;
        }
    }                   
    #asm("sei")
}



void RS485Message_Transmit(void)
{
    if ((RS485Send == 1)&&(RS485_MessageIndex == 0)&&((RS485_maxADR > 0)||(RFunprogramed == 1))&&(RS485_Timing == RS485_TimingMin)){
        RS485_MessageIndex=1;
        //if (RS485_IQnet){                                              
            //messageLength = messageRS485[LENGTH_B_IQ]+RS485Message_LengthIQ;
        //    messageLength = messageRS485[LENGTH_B]+RS485Message_LengthIQ;
        //}
        //else{ 
            messageLength = messageRS485TX[LENGTH_B]+RS485Message_Length;
        //}
        if (RS485NewMessage2Send == 1){
            RS485_MessageRepeat = 0;
            RS485NewMessage2Send = 0;   
        }
        RS485_TXEN = 1;
        UDR = 0;                        
    }
}

void RS485Message_Receive(void)
{
bit Receive = 1;
bit Start_message = 0; 
unsigned char Synchro = 0;
unsigned char index = 0;
unsigned char USART_Data = 0;
//unsigned char messageLength = 0;
unsigned char Length_Byte = 0;
unsigned char x = 0;
    if (gotRS485_message){    
        /*if (RS485_IQnet){                                              
            messageLength = RS485Message_LengthIQ;  
            Length_Byte = LENGTH_B_IQ; 
        }
        else{                      
            messageLength = RS485Message_Length;
            Length_Byte = LENGTH_B;
        }*/  
        messageLength = RS485Message_Length;
        Length_Byte = LENGTH_B;
        for(index = 0; index < (messageLength-1); ++index){
            messageRS485[index]=0;                       
        }      
        index = 0;
        TIMSK|=0x01;         //Zakazani preruseni pro timeout
        #asm("nop")
        #asm("nop")
        #asm("nop")
        TIFR=0x00;
        timeout = 0;
        TCCR0=0x05; 
        TCNT0=0x00;
        #asm("sei")  
        while(Receive){   
            USART_Data = 0;
            TCNT0=0x00;  
            USART_Data = USART_Receive();
            ++x;
            if (timeout == 0){
                if ((USART_Data == 0x00)&&(!Start_message)){
                    ++Synchro;
                    index=0;
                    if (Synchro > RS485SynchroNMB_old){ 
                        Receive = 0;     
                        gotRS485_message = 0; 
                    }             
                }
                else if ((USART_Data == START_SEQUENCE)&&(!Start_message)){
                    messageRS485[index]=USART_Data;              
                    Start_message = 1;
                    index=0;
                }
                else if (Start_message){
                    if (index < 32){
                        ++index;
                        messageRS485[index]=USART_Data;
                        if (index == Length_Byte){
                            messageLength = messageLength + messageRS485[index];
                        }
                    }
                    else{
                        Receive = 0;
                        gotRS485_message = 0;
                    } 
                }                              
                if (index==(messageLength-1)){
                    Receive = 0;     
                }            
            }       
            else{
                Receive = 0;
                gotRS485_message = 0;
            }                     
        }  
        timeout = 0;
        TCCR0=0x00;
        TIMSK&=0xFE;         //Zakazani preruseni pro timeout
        TIFR=0x00;
        #asm("cli")
    }   
    if (messageRS485[ADDR_B]==IQ_Marker){                                 
        RS485Message_DecodeIQ();
    }
    else{ 
        RS485Message_Decode();
    }   
}

void RS485Message_DecodeIQ(void)
{ 
unsigned char i = 0;
unsigned char CRC_Test = 0;
//unsigned char messageLength = 0;
    if (gotRS485_message){
        gotRS485_message = 0;
        messageLength = messageRS485[LENGTH_B]+RS485Message_Length;  
        if ((messageRS485[START_B]==START_SEQUENCE)&&(messageRS485[messageLength-1]==STOP_SEQUENCE)){
            for(i = 1; i < (messageLength-2); ++i){
                CRC_Test = messageRS485[i]^CRC_Test;                       
            }
            if (CRC_Test == messageRS485[messageLength-2]){ 
                i = messageRS485[ADDR_B_IQ]-1;
                Event = 1;
                switch (messageRS485[COMM_B_IQ]) {
                case 'F':
                    BitSet(RS485EventRAM[i],RS485_Fire_Alarm_bit);   
                    SPI_message2RF[SPI_ID] = NodeID_origin;
                    SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
                    SPI_message2RF[SPI_ZONE] = ZoneID_origin;
                    SPI_message2RF[SPI_COMMAND] = 'F';
                    RS485_timer = 0;
                break;
                case 'S':               
                    if (BitTest(RS485EventRAM[i],RS485_Fire_Alarm_bit)==1){ 
                        SPI_message2RF[SPI_ID] = NodeID_origin;
                        SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
                        SPI_message2RF[SPI_ZONE] = ZoneID_origin;
                        SPI_message2RF[SPI_COMMAND] = 'S';            
                    }
                    BitRST(RS485EventRAM[i],RS485_Fire_Alarm_bit);
                    RS485_timer = 0;
                break;
                case 'E':
                    BitSet(RS485EventRAM[i],RS485_Fault_Alarm_bit);
                    RS485_timer = 0;
                break;
                case 'C':
                    BitRST(RS485EventRAM[i],RS485_Fault_Alarm_bit);
                    BitRST(RS485EventRAM[i],RS485_Bat_Fault_bit);
                    RS485_timer = 0;
                break;
                case 'R':
                    if (BitTest(RS485EventRAM[i],RS485_Fire_Alarm_bit)==1){  
                        SPI_message2RF[SPI_ID] = NodeID_origin;
                        SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
                        SPI_message2RF[SPI_ZONE] = ZoneID_origin;
                        SPI_message2RF[SPI_COMMAND] = 'R';            
                    }
                    RS485EventRAM[i]=0;
                    RS485_timer = 0; 
                    if (RFunprogramed==1){
                        for(i = 0; i<32; ++i){
                            BitRST(BackboneEventRAM[i],Fire_Alarm_bit);
                            BitRST(LoopEventRAM[i],Fire_Alarm_bit);    
                        } 
                        CLR_INT_SIREN_SW;
                    } 
                break;
                case 'K':
                    BitRST(RS485EventRAM[i],RS485_comm_det_bit); 
                    /*if (messageRS485[ADDR_B_IQ]<RS485_IQnetADR){ //synchronisation
                        RS485_timer = RS485_timer_MAX - RS485_time2ANSWER;
                    }*/
                break;
                //default:
                
               // break;
                };        
            }
        }
    }
}

void RS485Message_Decode(void)
{ 
unsigned char i = 0;
unsigned char CRC_Test = 0;
//unsigned char messageLength = 0;
    if (gotRS485_message){
        gotRS485_message = 0;
        messageLength = messageRS485[LENGTH_B]+RS485Message_Length;  
        if ((messageRS485[START_B]==START_SEQUENCE)&&(messageRS485[messageLength-1]==STOP_SEQUENCE)){
            for(i = 1; i < (messageLength-2); ++i){
                CRC_Test = messageRS485[i]^CRC_Test;                       
            }
            if (CRC_Test == messageRS485[messageLength-2]){ 
                i = messageRS485[ADDR_B]-1;
                Event = 1;
                switch (messageRS485[COMM_B]) {
                case 'F':
                    BitSet(RS485EventRAM[i],RS485_Fire_Alarm_bit);
                    SPI_message2RF[SPI_ID] = NodeID_origin;
                    SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
                    SPI_message2RF[SPI_ZONE] = ZoneID_origin;   
                    SPI_message2RF[SPI_COMMAND] = 'F';
                break;
                case 'E':
                    BitSet(RS485EventRAM[i],RS485_Fault_Alarm_bit);
                break;
                case 'S':               
                    BitRST(RS485EventRAM[i],RS485_Fault_Alarm_bit);
                break;
                case 'R':
                    if (BitTest(RS485EventRAM[i],RS485_Fire_Alarm_bit)==1){
                        SPI_message2RF[SPI_ID] = NodeID_origin;
                        SPI_message2RF[SPI_SUBLOOP] = SystemID_origin;
                        SPI_message2RF[SPI_ZONE] = ZoneID_origin;
                        SPI_message2RF[SPI_COMMAND] = 'S';            
                    }   
                    BitRST(RS485EventRAM[i],RS485_Fire_Alarm_bit);
                    BitRST(RS485EventRAM[i],RS485_Fault_Alarm_bit);
                    //ResetCPU();
                break;
                case 'T':
                    BitRST(RS485EventRAM[i],RS485_comm_det_bit);
                break;
                //default:
                
               // break;
                };        
            }
        }
    }
}

unsigned char USART_Receive(void)
{              
    while ((!(UCSRA & (1<<RXC)))&&(timeout==0));
    return UDR;
}

//void USART_Transmit(unsigned char data)
//{   
//    while (!(UCSRA & (1<<UDRE)));
//    UDR = data;
//}
               
unsigned char RS485Comm_Test(unsigned char adr2Test)
{
unsigned char i;
    //if ((RS485Send == 1)||(RS485_maxADR==0)){
    if ((RS485_timer == RS485_timer_MAX)&&(RS485_maxADR > 1)){
        RS485_timer -= RS485_time2ANSWER;
        ++adr2Test;
        RS485TestEN = 1;
    }
    if (RS485TestEN){
        RS485TestEN = 0;
        if (RS485_IQnet){
            RS485_timer = 0; 
            if (adr2Test == RS485CommTestTimeout/2){  
                RS485Message_Construct(NodeID_origin,SystemID_origin,ZoneID_origin,'K');
                BitRST(RS485EventRAM[RS485_IQnetADR-1],RS485_comm_det_bit);  
            }
            if (adr2Test>RS485CommTestTimeout){ 
                adr2Test = 0;
                RS485_timer = 0;    
                RS485_Comm_Fault = 0; 
                for(i = 0; i<RS485_maxADR;++i){
                    if (BitTest(RS485EventRAM[i],RS485_comm_det_bit)==1){
                        RS485_Comm_Fault = 1;    
                    }
                    else{
                        BitSet(RS485EventRAM[i],RS485_comm_det_bit);
                    }
                } 
                /*if (RS485_Comm_Fault){
                    RS485Message_Construct(NodeID_origin,SystemID_origin,ZoneID_origin,RS485_maxADR);
                }*/
            }
        }
        else{
            messageRS485TX[START_B]=START_SEQUENCE;
            messageRS485TX[STOP_B]=STOP_SEQUENCE;
            messageRS485TX[ADDR_B]=adr2Test;
            messageRS485TX[LENGTH_B]=0;
            messageRS485TX[COMM_B]='T';
            messageRS485TX[CRC_B]=messageRS485TX[ADDR_B]^messageRS485TX[LENGTH_B]^messageRS485TX[COMM_B];
            RS485NewMessage2Send = 1;
            RS485_Timing = RS485_TimingMin;
            RS485Send = 1;
            if (adr2Test>(RS485_maxADR+1)){
                adr2Test = 0;
                RS485_timer = 0;             
                RS485_Comm_Fault = 0;
                for(i = 0; i<RS485_maxADR;++i){
                    if (BitTest(RS485EventRAM[i],RS485_comm_det_bit)==1){
                        RS485_Comm_Fault = 1;
                    }
                    else{
                        BitSet(RS485EventRAM[i],RS485_comm_det_bit);
                    }
                }
            }    
        }        
    }
    return(adr2Test);  
}
void delay_msWDR(unsigned char MS){
unsigned char z;
unsigned char y; 
unsigned char x;                                  
    for (x = 0;x < MS;++x){   //aprox number of ms = exact 1MS = 1.049ms
        for (y = 0;y < 3;++y){   //aprox 1ms = exact 1.025ms
            for (z = 0;z < 228;++z){   //aprox 350us
                #asm("nop");
            }
        }                                       
    }
}


unsigned char RS485Message_Construct(unsigned char T_ID,unsigned char T_SUB,unsigned char T_ZONE,unsigned char dataComm)
{
    if ((RS485_maxADR > 0)||(ProductionMode == 1)){
        if (RS485_IQnet){
            messageRS485TX[START_B]=START_SEQUENCE; 
            messageRS485TX[ADDR_B]=IQ_Marker;
            messageRS485TX[LENGTH_B]=RS485Message_LengthIQ;
            messageRS485TX[STOP_B_IQ]=STOP_SEQUENCE;
            messageRS485TX[LOOP_ID_IQ]=0;                    
            messageRS485TX[DET_ID_IQ]=T_ID;
           
            messageRS485TX[ZONE_ID_IQ]=T_ZONE;
            messageRS485TX[SUBLOOP_ID_IQ]=T_SUB;                
            messageRS485TX[ADDR_B_IQ]=RS485_IQnetADR;
            messageRS485TX[COMM_B_IQ]=dataComm;
            messageRS485TX[CRC_B_IQ]=messageRS485TX[ADDR_B]^messageRS485TX[LENGTH_B]^messageRS485TX[LOOP_ID_IQ]^messageRS485TX[DET_ID_IQ]^messageRS485TX[ZONE_ID_IQ]^messageRS485TX[SUBLOOP_ID_IQ]^messageRS485TX[ADDR_B_IQ]^messageRS485TX[COMM_B_IQ];
        }
        else{
            messageRS485TX[START_B]=START_SEQUENCE;
            messageRS485TX[STOP_B]=STOP_SEQUENCE;
            messageRS485TX[ADDR_B]=RS485_maxADR+1;
            messageRS485TX[LENGTH_B]=0;
            messageRS485TX[COMM_B]=dataComm;
            messageRS485TX[CRC_B]=messageRS485TX[ADDR_B]^messageRS485TX[LENGTH_B]^messageRS485TX[COMM_B];
        } 
        RS485_Timing = RS485_TimingMin;  
        RS485NewMessage2Send = 1;
        RS485Send = 1;        
    } 
    else{
        RS485Send = 0;
    }
    return(RS485Send);              
}

void RFEventSave(unsigned char E_ID,unsigned char E_SUBLOOP,unsigned char E_ZONE, unsigned char E_COMMAND)
{
unsigned int addr,x;
unsigned char disable;
unsigned char E_ID2,E_SUBLOOP2,E_ZONE2,E_COMMAND2,E_COUNT;
    //EEPROMRFEventDATA      
    addr = 0;
    addr = (EEPROM_read(EEPROM_store+EEadrHRFEvent)<<8)&0xFF00;
    addr = addr +EEPROM_read(EEPROM_store+EEadrLRFEvent);
    if (addr > 0x01DB){
        addr = EEPROMRFEventDATA;
    }
    else{
        addr = addr + EEPROMRFEventDATA;
    }       
    x = EEPROMRFEventDATA;    
    disable = 0;
    while((x<addr)&&(disable == 0)){
        E_ID2 = EEPROM_read(x);
        ++x;                   
        E_SUBLOOP2 = EEPROM_read(x);
        ++x;
        E_ZONE2 = EEPROM_read(x);
        ++x;
        E_COMMAND2 = EEPROM_read(x);
        ++x; 
        E_COUNT = EEPROM_read(x); 
        ++x;
        if ((E_ID2==E_ID)&&(E_SUBLOOP2==E_SUBLOOP)&&(E_ZONE2==E_ZONE)&&(E_COMMAND2 == E_COMMAND)){
            disable = 1;   
            if (E_COUNT<0xFF){
            	E_COUNT = E_COUNT+1;
            }
            addr = x - 1;
        }              
    }                  
    if (disable == 0){
        EEPROM_write(addr, E_ID);
        ++addr;
        EEPROM_write(addr, E_SUBLOOP);
        ++addr;
        EEPROM_write(addr, E_ZONE);
        ++addr;
        EEPROM_write(addr, E_COMMAND);
        ++addr;
        EEPROM_write(addr, 0x01);
        ++addr;          
        addr = addr-EEPROMRFEventDATA;
        x = addr>>8;
        x = x&(0x00FF); 
        EEPROM_write(EEPROM_store+EEadrHRFEvent,x);    
        x = addr;      
        x = x&(0x00FF);
        EEPROM_write(EEPROM_store+EEadrLRFEvent,x);
    }
    else
    {
        if (E_COUNT<0xFF){
        	EEPROM_write(addr, E_COUNT);
        }    
    }
}

void RFEventsDelete()
{
unsigned int addr;
unsigned char x;
    //EEPROMRFEventDATA      
    addr = EEPROMRFEventDATA;
    while(addr < 0x01DC){
        #asm("wdr");
        EEPROM_write(addr,0xFF);
        ++addr;    
    }
    addr = 0x0000;
    x = addr>>8; 
    EEPROM_write(EEPROM_store+EEadrHRFEvent,x);    
    x = addr; 
    EEPROM_write(EEPROM_store+EEadrLRFEvent,x);
}

void EEPROM_write(unsigned int addr, unsigned char data)
{
    /*Wait for completion of previous write*/
    while(EECR & (1<<EEWE));
    /* Set up address and data registers */
    EEAR = addr;
    EEDR = data;
    /* 
    Write logical one to EEMWE */
    EECR |= (1<<EEMWE);
    /* Start eeprom write by setting EEWE */
    EECR |= (1<<EEWE);
}

unsigned char EEPROM_read(unsigned int addr)
{
    /* Wait for completion of previous write */
    while(EECR & (1<<EEWE));
    /* Set up address register */
    EEAR = addr;
    /* Start eeprom read by writing EERE */
    EECR |= (1<<EERE);
    /* Return data from data register */
    return EEDR;
}

void Check_Memory_Integrity(void){  //Memory check takes 33ms to complete
    if (MemTestEN == 1){
       MemTestEN = 0; 
       Actual_memory_CRC=0;
       //Z register backup   
       #asm
           ldi R31,0x02            ;address of ZalohaZ[0]
           ldi R30,0x29            ;address of ZalohaZ[0]
           st Z+,R29
           st Z+,R28
           st Z+,R27
           st Z+,R26
      #endasm             
      //-----------------
      //Load the CRC into Actual_memory_CRC 
      #asm 
          ldi R31,0
          ldi R30,0
          clr R0
          test_memory_begin:
          lpm R1,Z 
          eor R0,R1
          test_memory_loop:  
          inc R30
          lpm R1,Z 
          eor R0,R1
          cpi R30,0xFF
          brne test_memory_loop 
          test_memory_loop2:   
          inc R31
          ldi R30,0
          cpi R31,0x40
          brne test_memory_begin
          nop
          sts 0x0227,R0     
          //ST   -Y,R26
        //  MOV  R30,R0
        //  ST   Y,R30
      #endasm; 
      //-----------------------------------          
      //Z register recovery
      #asm
          ldi R31,0x02           ;address of ZalohaZ[0]
          ldi R30,0x29           ;address of ZalohaZ[0]
          ld R29,Z+
          ld R28,Z+
          ld R27,Z+
          ld R26,Z+
      #endasm              
      //-------------------      
    }
}
